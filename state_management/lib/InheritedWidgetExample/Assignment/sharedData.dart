import 'package:flutter/material.dart';

class SharedData extends InheritedWidget {
  final Map<String, String> data;

  const SharedData({
    super.key,
    required this.data,
    required super.child,
  });

  @override
  bool updateShouldNotify(SharedData oldWidget) {
    return data != oldWidget.data;
  }

  static SharedData of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<SharedData>()!;
  }
}
