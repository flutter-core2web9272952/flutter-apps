import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Image.network(
              "https://st2.depositphotos.com/2001755/8564/i/450/depositphotos_85647140-stock-photo-beautiful-landscape-with-birds.jpg"),
          height: 200,
          width: 200,
          color: Colors.red,
        ),
      ),
    );
  }
}
