import 'package:flutter/material.dart';

class Assignment5 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Core2web")]),
        actions: [Icon(Icons.qr_code_2_outlined)],
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Container(
              width: 250,
              height: 150,
              child: Image.asset(
                  "/home/sandy/Desktop/Study/Flutter/flutter/Projects/appbar_container/assets/img/first.jpg")),
          Container(
              width: 250,
              height: 150,
              child: Image.asset(
                  "/home/sandy/Desktop/Study/Flutter/flutter/Projects/appbar_container/assets/img/second.jpg")),
          Container(
              width: 250,
              height: 150,
              child: Image.asset(
                  "/home/sandy/Desktop/Study/Flutter/flutter/Projects/appbar_container/assets/img/third.jpg"))
        ]),
      ),
    );
  }
}
