import 'package:flutter/material.dart';
import 'package:state_management/InheritedWidgetExample/Assignment1/model.dart';

class SharedData extends InheritedWidget {
  Employee data;

  SharedData({
    super.key,
    required this.data,
    required super.child,
  });

  @override
  bool updateShouldNotify(SharedData oldWidget) {
    return data != oldWidget.data;
  }

  static SharedData of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<SharedData>()!;
  }
}
