import 'package:flutter/material.dart';

class Assignment3 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: const BorderRadius.all(Radius.circular(20)),
            border: Border.all(color: Colors.blue, width: 5),
            gradient: const LinearGradient(
              stops: [0.1, 0.5],
              colors: [Colors.red, Colors.green],
            ),
          ),
          height: 200,
          width: 200,
        ),
      ),
    );
  }
}
