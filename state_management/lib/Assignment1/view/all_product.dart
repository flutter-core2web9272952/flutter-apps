import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:state_management/Assignment1/controllers/product_controller.dart';
import 'package:state_management/Assignment1/controllers/wishlist_controller.dart';
import 'package:state_management/Assignment1/view/wishlist_screen.dart';

class AllProducts extends StatefulWidget {
  const AllProducts({super.key});

  @override
  State<StatefulWidget> createState() {
    return _AllProductsState();
  }
}

class _AllProductsState extends State {
  @override
  Widget build(BuildContext context) {
    var productControllerObj = Provider.of<ProviderProductController>(context);
    var productWishlisObj = Provider.of<ProviderWishlistController>(context);

    return Scaffold(
      appBar: AppBar(
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const WishList()));
            },
            child: const Padding(
                padding: EdgeInsets.only(right: 10),
                child: Icon(Icons.favorite)),
          )
        ],
      ),
      body: Consumer(
        builder: (create, value, child) {
          return ListView.builder(
            itemCount: productControllerObj.data.length,
            itemBuilder: (context, index) {
              return Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 25),
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 25),
                alignment: Alignment.bottomCenter,
                width: 300,
                height: 480,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                    bottomRight: Radius.circular(5),
                    bottomLeft: Radius.circular(5),
                  ),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 0),
                        spreadRadius: 0,
                        blurRadius: 10,
                        color: Color.fromARGB(94, 0, 0, 0))
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Image.network(
                      productControllerObj.data[index].productImg,
                      width: 260,
                      height: 320,
                      fit: BoxFit.cover,
                    ),
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          productControllerObj.data[index].productName,
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 3),
                          decoration: BoxDecoration(
                            color: const Color.fromARGB(101, 255, 235, 59),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Text(
                            "\$ ${productControllerObj.data[index].productPrice}",
                            style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Consumer(
                          builder: (context, value, child) {
                            return IconButton(
                              onPressed: () {
                                productControllerObj
                                    .toggleFevoriteSingleProduct(index);

                                if (productControllerObj
                                    .data[index].isfavorite) {
                                  productWishlisObj.addToWishList(
                                      product:
                                          productControllerObj.data[index]);
                                } else {
                                  productWishlisObj.removeFromWishList(
                                      product:
                                          productControllerObj.data[index]);
                                }
                              },
                              icon: productControllerObj.data[index].isfavorite
                                  ? const Icon(Icons.favorite)
                                  : const Icon(Icons.favorite_outline),
                            );
                          },
                        ),
                        Row(
                          children: [
                            GestureDetector(
                              onTap: () {
                                productControllerObj
                                    .decrementQuantitySingleProduct(index);
                              },
                              child: const Icon(Icons.remove),
                            ),
                            const SizedBox(width: 5),
                            Consumer(
                              builder: (create, value, child) {
                                return Text(
                                  "${productControllerObj.data[index].quantity}",
                                );
                              },
                            ),
                            const SizedBox(width: 5),
                            GestureDetector(
                              onTap: () {
                                productControllerObj
                                    .incrementQuantitySingleProduct(index);
                              },
                              child: const Icon(Icons.add),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }
}
