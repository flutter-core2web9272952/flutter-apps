import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management/Assignment1/controllers/product_controller.dart';
import 'package:state_management/Assignment1/models/product_model.dart';
import 'package:state_management/Assignment1/view/all_product.dart';
import 'package:state_management/Assignment1/view/display_product.dart';

class ProductDetails extends StatefulWidget {
  const ProductDetails({super.key});

  @override
  State createState() {
    return _ProductDetailsState();
  }
}

class _ProductDetailsState extends State {
  final TextEditingController _imgController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _priceController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) {
                return const AllProducts();
              }));
            },
            child: const Padding(
              padding: EdgeInsets.only(right: 10),
              child: Icon(Icons.shopping_bag),
            ),
          )
        ],
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 20,
          ),
          const Text(
            "Add Product",
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
          ),
          const SizedBox(height: 50),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 260,
                child: TextFormField(
                  controller: _imgController,
                  decoration: const InputDecoration(
                    hintText: "Product img URL",
                    border: OutlineInputBorder(),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 260,
                child: TextFormField(
                  controller: _nameController,
                  decoration: const InputDecoration(
                    hintText: "Product Name",
                    border: OutlineInputBorder(),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 30),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: 260,
                child: TextFormField(
                  controller: _priceController,
                  decoration: const InputDecoration(
                    hintText: "Product price",
                    border: OutlineInputBorder(),
                  ),
                ),
              )
            ],
          ),
          const SizedBox(height: 30),
          GestureDetector(
            onTap: () {
              Provider.of<ProviderProductController>(context, listen: false)
                  .addProduct(
                img: _imgController.text.trim(),
                name: _nameController.text.trim(),
                price: _priceController.text.trim(),
              );

              _imgController.text = "";
              _nameController.text = "";
              _priceController.text = "";

              // Provider.of<ProviderProductController>(context, listen: false)
              //     .changeProductInfo(
              //   img: _imgController.text.trim(),
              //   name: _nameController.text,
              //   price: _priceController.text,
              // );

              // Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              //   return const DisplyProduct();
              // }));
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              decoration: BoxDecoration(
                color: Colors.purple.shade100,
                borderRadius: BorderRadius.circular(10),
              ),
              child: const Text(
                "Add",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
              ),
            ),
          )
        ],
      ),
    );
  }
}
