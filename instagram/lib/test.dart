import 'package:flutter/material.dart';
import 'dart:ui'; // Import this for ImageFilter

class GlassContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Glass Container'),
      ),
      // Apply a backdrop filter to create the glass-like effect
      backgroundColor:
          Colors.blue, // Background color for better visibility of the effect
      body: Center(
        child: BackdropFilter(
          filter:
              ImageFilter.blur(sigmaX: 10, sigmaY: 10), // Adjust blur intensity
          child: Container(
            color: Colors.white
                .withOpacity(0.2), // Adjust opacity for the glass effect
            width: 200,
            height: 200,
            child: Center(
              child: Text(
                'Glass Container',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
