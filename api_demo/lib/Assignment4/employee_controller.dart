import 'package:api_demo/Assignment4/employee_model.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:developer';

class EmployeeController extends ChangeNotifier {
  Data? data;

  Future<void> getEmployeeData({required String id}) async {
    log("message");
    Uri url =
        Uri.parse("https://dummy.restapiexample.com/api/v1/employee/${id}");
    http.Response response = await http.get(url);
    var responseData = json.decode(response.body);

    data = Data(responseData['data']);

    log("${data}");

    notifyListeners();
  }

  Future<void> postEmployeeData() async {
    log("message");
    Uri url = Uri.parse("https://dummy.restapiexample.com/api/v1/create");

    Map<String, dynamic> data = {
      "name": "Sandesh Marathe",
      "salary": "123",
      "age": "23",
    };

    http.Response response = await http.post(url, body: data);

    log(response.body);
  }
}
