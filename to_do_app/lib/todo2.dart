import 'package:flutter/material.dart';

class ToDo extends StatefulWidget {
  const ToDo({super.key});

  @override
  State<StatefulWidget> createState() => _DisplayList();
}

class _DisplayList extends State {
  List<int> list = [];
  int cnt = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("LISTVIEW"),
      ),
      body: ListView.builder(
          itemCount: list.length,
          itemBuilder: (context, index) {
            return Container(
              margin: const EdgeInsets.all(5),
              width: double.infinity,
              height: 40,
              decoration: const BoxDecoration(
                gradient:
                    LinearGradient(colors: [Colors.green, Colors.lightGreen]),
              ),
              alignment: Alignment.center,
              child: Text(
                "${list[index]}",
                style:
                    const TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
              ),
            );
          }),
      floatingActionButton: FloatingActionButton(
        hoverColor: Colors.green,
        tooltip: "Add element in list",
        splashColor: const Color.fromARGB(255, 18, 71, 19),
        onPressed: () {
          setState(() {
            cnt++;
            list.add(cnt);
          });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
