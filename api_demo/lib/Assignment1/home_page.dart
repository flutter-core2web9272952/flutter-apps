import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:developer';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State {
  List<dynamic> empData = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("API Binding"),
      ),
      body: ListView.builder(
        itemCount: empData.length,
        itemBuilder: (context, index) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(empData[index]['employee_name']),
              const SizedBox(
                width: 30,
              ),
              Text("${empData[index]['employee_salary']}")
            ],
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          getEmployeeData();
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  void getEmployeeData() async {
    Uri url = Uri.parse("https://dummy.restapiexample.com/api/v1/employees");
    http.Response response = await http.get(url);
    var responseData = json.decode(response.body);

    setState(() {
      empData = responseData['data'];
    });
  }
}


/*

  Main code

  class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}


*/