import 'package:flutter/material.dart';

class Assignment4 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          decoration: BoxDecoration(
              color: Colors.purple,
              borderRadius: const BorderRadius.all(Radius.circular(20)),
              border: Border.all(
                  color: const Color.fromARGB(255, 220, 109, 240), width: 5),
              boxShadow: const [
                BoxShadow(
                  color: Colors.purple,
                  offset: Offset(5, 0),
                  blurRadius: 40,
                ),
                BoxShadow(
                  color: Colors.purple,
                  offset: Offset(0, 5),
                  blurRadius: 40,
                ),
                BoxShadow(
                  color: Colors.purple,
                  offset: Offset(0, -5),
                  blurRadius: 40,
                ),
                BoxShadow(
                  color: Colors.purple,
                  offset: Offset(-5, 0),
                  blurRadius: 40,
                ),
              ]),
          height: 200,
          width: 200,
        ),
      ),
    );
  }
}
