import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:url_launcher/url_launcher.dart';

class MoreInfoPage extends StatefulWidget {
  final dynamic object;

  const MoreInfoPage({super.key, required this.object});

  @override
  State<MoreInfoPage> createState() {
    return _MoreInfoPageState();
  }
}

class _MoreInfoPageState extends State<MoreInfoPage> {
  Future<void> _launchUrl(String str) async {
    final Uri _url = Uri.parse(str);

    if (Platform.isLinux) {
      Process.run('xdg-open', [_url.toString()]);
    } else {
      await launchUrl(_url);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.grey,
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              child: FadeInImage(
                  fit: BoxFit.cover,
                  placeholder: NetworkImage(widget.object.img),
                  image: NetworkImage(
                    widget.object.bigImg,
                  )),
            ),
            const SizedBox(
              height: 30,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  widget.object.name,
                  style: GoogleFonts.luckiestGuy(
                    fontSize: 24,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                widget.object.moreInfo,
                textAlign: TextAlign.justify,
                style: GoogleFonts.zenDots(
                  fontSize: 16,
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: () {
                    _launchUrl(widget.object.websiteLink);
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 16, horizontal: 30),
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      gradient: LinearGradient(
                        begin: Alignment.bottomLeft,
                        end: Alignment.topRight,
                        colors: [
                          Color.fromRGBO(137, 30, 221, 1),
                          Color.fromRGBO(223, 60, 28, 1),
                        ],
                      ),
                    ),
                    child: Text(
                      "Read More",
                      textAlign: TextAlign.justify,
                      style: GoogleFonts.zenDots(
                        fontSize: 20,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 30,
            ),
          ],
        ),
      ),
    );
  }
}
