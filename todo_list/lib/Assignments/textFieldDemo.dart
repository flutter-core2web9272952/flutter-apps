import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State {
  Map<String, String> compImages = {
    "nvidia": "https://logowik.com/content/uploads/images/nvidia3433.jpg",
    "intel":
        "https://thumbs.dreamstime.com/b/los-angeles-california-usa-january-intel-logo-white-background-illustrative-editorial-los-angeles-california-usa-january-intel-169595618.jpg",
    "microsoft":
        "https://i.pinimg.com/originals/b2/d9/06/b2d906c4b90768b63db1078cf365ca9f.jpg",
    "google":
        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRWLpce6s2cc9rOc1JtGNp1mz8KCmkTALzZmQ&usqp=CAU",
    "amd": "https://logowik.com/content/uploads/images/amd7686.jpg",
    "apple":
        "https://media.licdn.com/dms/image/D4D12AQHwi4jdRd3fQQ/article-cover_image-shrink_600_2000/0/1685279753620?e=2147483647&v=beta&t=7I-pJ0kDQfNl4w-0Ue8aPyol_X-aWOQlzp18NhTldys",
    "tesla":
        "https://thumbs.dreamstime.com/b/tesla-logo-white-background-editorial-illustrative-printed-white-paper-logo-eps-vector-tesla-logo-white-background-editorial-206666007.jpg"
  };

  List<Map> data = [];

  TextEditingController nameController = TextEditingController();
  TextEditingController compNameController = TextEditingController();
  TextEditingController locationController = TextEditingController();

  bool showFlg = false;
  bool isClicked = false;

  String? imgString =
      "https://wallpapers.com/images/featured/blank-white-7sn5o1woonmklx1h.jpg";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Text("Add"),
      ),
      appBar: AppBar(
        title: const Text("LoginPage"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: Column(
        children: [
          Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 40,
                ),
                SizedBox(
                  width: 300,
                  child: TextField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      hintText: "Enter Name",
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: 300,
                  child: TextField(
                    controller: compNameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      hintText: "Enter Company Name",
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: 300,
                  child: TextField(
                    controller: locationController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(30))),
                      hintText: "Enter Location",
                    ),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  style: const ButtonStyle(
                      fixedSize: MaterialStatePropertyAll(Size(300, 40))),
                  onPressed: () {
                    isClicked = true;

                    if (nameController.text.isEmpty ||
                        compNameController.text.isEmpty ||
                        locationController.text.isEmpty) {
                      showFlg = false;
                    } else {
                      showFlg = true;

                      print(compNameController.text.toLowerCase());

                      if (compImages
                          .containsKey(compNameController.text.toLowerCase())) {
                        imgString =
                            compImages[compNameController.text.toLowerCase()];

                        print(imgString);
                      } else {
                        imgString =
                            "https://wallpapers.com/images/featured/blank-white-7sn5o1woonmklx1h.jpg";
                      }

                      data.add({
                        "name": nameController.text,
                        "company": compNameController.text,
                        "location": locationController.text,
                        "imgString": imgString,
                      });
                    }

                    setState(() {});
                  },
                  child: const Text(
                    "Add Data",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                ElevatedButton(
                  style: const ButtonStyle(
                      fixedSize: MaterialStatePropertyAll(Size(300, 40))),
                  onPressed: () {
                    nameController.text = "";
                    compNameController.text = "";
                    locationController.text = "";
                  },
                  child: const Text(
                    "Clear Data",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w600),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                !showFlg
                    ? SizedBox(
                        child: isClicked
                            ? const Text(
                                "Fill data first...!!",
                                style: TextStyle(
                                    color: Color.fromRGBO(244, 67, 54, 1),
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700),
                              )
                            : const SizedBox(),
                      )
                    : const SizedBox()
              ],
            ),
          ),
          const Divider(
            thickness: 5.5,
          ),
          Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: data.length,
              itemBuilder: (context, index) {
                return Container(
                  margin: EdgeInsets.all(10),
                  width: 500,
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(border: Border.all(width: 1)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Text(
                                "Name : ",
                                style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              Text(data[index]["name"],
                                  style: const TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w500,
                                  )),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Text("Company Name : ",
                                  style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w500,
                                  )),
                              Text(data[index]["company"],
                                  style: const TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w500,
                                  ))
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Text("Location : ",
                                  style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w500,
                                  )),
                              Text(data[index]["location"],
                                  style: const TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.w500,
                                  ))
                            ],
                          )
                        ],
                      ),
                      const SizedBox(
                        width: 30,
                      ),
                      Image.network(
                        "${data[index]["imgString"]}",
                        width: 200,
                      ),
                    ],
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }
}

/*

 
*/
