import 'package:flutter/material.dart';
import 'package:quiz_app/lifecycle.dart';
// import 'package:quiz_app/quizApp_final_ver.dart';
// import 'HomeScreen.dart';
import 'QuizApp.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: LifeCycle(),
      debugShowCheckedModeBanner: false,
    );
  }
}
