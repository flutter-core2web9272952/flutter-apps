import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State<StatefulWidget> createState() {
    return _QuizAppState();
  }
}

class _QuizAppState extends State {
  int qIndx = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Quiz App"),
        centerTitle: true,
      ),
      body: Column(
        // crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 20),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Question :",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              ),
              const SizedBox(
                width: 5,
              ),
              Text("$qIndx/10"),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Question :",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
              ),
              const Text("What is Flutter?"),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(onPressed: () {}, child: const Text("Option A")),
              SizedBox(
                height: 10,
              ),
              ElevatedButton(onPressed: () {}, child: const Text("Option A")),
              SizedBox(
                height: 10,
              ),
              ElevatedButton(onPressed: () {}, child: const Text("Option A")),
              SizedBox(
                height: 10,
              ),
              ElevatedButton(onPressed: () {}, child: const Text("Option A")),
            ],
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            qIndx = (++qIndx) % 10;
          });
        },
        child: const Text("Next"),
      ),
    );
  }
}
