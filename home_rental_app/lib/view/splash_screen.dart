import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:home_rental_app/view/home_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
              "assets/img3.jpg",
              fit: BoxFit.cover,
              height: MediaQuery.of(context).size.height * 7 / 10,
              width: MediaQuery.of(context).size.width,
            ),
            const SizedBox(height: 20),
            Text(
              "Lets find your Paradise",
              style: GoogleFonts.poppins(
                fontSize: 22,
                fontWeight: FontWeight.w600,
              ),
            ),
            const SizedBox(height: 12),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              child: Text(
                "Find your perfect dream space with just a few clicks",
                textAlign: TextAlign.center,
                style: GoogleFonts.poppins(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                    color: const Color.fromRGBO(101, 101, 101, 1)),
              ),
            ),
            const SizedBox(height: 13),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return HomeScreen();
                    },
                  ),
                );
              },
              child: Container(
                padding: const EdgeInsets.symmetric(
                  horizontal: 47,
                  vertical: 11,
                ),
                decoration: BoxDecoration(
                  color: const Color.fromRGBO(32, 169, 247, 1),
                  borderRadius: BorderRadius.circular(40),
                ),
                child: Text(
                  "Get Started",
                  style: GoogleFonts.poppins(
                      fontSize: 22,
                      fontWeight: FontWeight.w400,
                      color: Colors.white),
                ),
              ),
            ),
            SizedBox(
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).size.height * 95 / 100,
            ),
            Container(
              width: 138.0,
              height: 5.15,
              decoration: BoxDecoration(
                color: Colors.black,
                borderRadius: BorderRadius.circular(10.0),
              ),
            ),
            const SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
