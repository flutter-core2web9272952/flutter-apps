import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quiz_app_advance/model/cateogory_model.dart';
import 'package:quiz_app_advance/view/quiz_screen.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State {
  List<CategoryModal> data = [
    CategoryModal(
      icon: "M",
      categoryName: "Math",
      iconColor: const Color.fromRGBO(200, 60, 0, 1),
    ),
    CategoryModal(
      icon: "H",
      categoryName: "History",
      iconColor: const Color.fromRGBO(255, 157, 66, 1),
    ),
    CategoryModal(
      icon: "G",
      categoryName: "Geography",
      iconColor: const Color.fromRGBO(3, 163, 134, 1),
    ),
    CategoryModal(
      icon: "B",
      categoryName: "Biology",
      iconColor: const Color.fromRGBO(251, 43, 255, 1),
    ),
    CategoryModal(
      icon: "S",
      categoryName: "Sports",
      iconColor: const Color.fromRGBO(45, 104, 255, 1),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 27, vertical: 50),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Hi Pamela",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.dmSans(
                          fontSize: 25,
                          fontWeight: FontWeight.w700,
                          color: Color.fromRGBO(131, 76, 52, 1)),
                    ),
                    Text(
                      "Great to see you again!",
                      style: GoogleFonts.dmSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: Color.fromRGBO(131, 76, 52, 1)),
                    )
                  ],
                ),
                Container(
                  width: 64,
                  height: 64,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(250, 188, 154, 1)),
                )
              ],
            ),
            const SizedBox(
              height: 40,
            ),
            ListView.separated(
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => const QuizPage()),
                      );
                    },
                    child: ListTile(
                      contentPadding: const EdgeInsets.symmetric(
                        vertical: 15,
                        horizontal: 20,
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15)),
                      leading: Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 10),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(8),
                        ),
                        child: Text(
                          data[index].icon,
                          style: GoogleFonts.dmSans(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: data[index].iconColor,
                          ),
                        ),
                      ),
                      title: Text(
                        data[index].categoryName,
                        style: GoogleFonts.dmSans(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            color: const Color.fromRGBO(131, 76, 52, 1)),
                      ),
                      tileColor: const Color.fromRGBO(255, 237, 217, 1),
                      trailing: const Icon(Icons.arrow_forward_ios),
                    ),
                  );
                },
                separatorBuilder: (contex, index) {
                  return const SizedBox(
                    height: 20,
                  );
                },
                itemCount: data.length)
          ],
        ),
      ),
    );
  }
}
