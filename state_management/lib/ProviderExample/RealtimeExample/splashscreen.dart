import 'package:flutter/material.dart';
import 'package:state_management/ProviderExample/RealtimeExample/homescreen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  State createState() => _SplashScreenState();
}

class _SplashScreenState extends State {
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) => const HomePage(),
            ),
          );
        },
        child: Image.network(
            "https://i.pinimg.com/564x/37/70/2b/37702b3bb77b4e25d2f5112ad82168c5.jpg"),
      ),
    ));
  }
}


/*

ipl logo : https://i.pinimg.com/564x/37/70/2b/37702b3bb77b4e25d2f5112ad82168c5.jpg


*/