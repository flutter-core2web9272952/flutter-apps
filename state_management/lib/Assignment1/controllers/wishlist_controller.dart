import 'package:flutter/material.dart';
import 'package:state_management/Assignment1/models/product_model.dart';

class ProviderWishlistController with ChangeNotifier {
  List<ProductModel> data = [];

  void addToWishList({required ProductModel product}) {
    data.add(product);
    notifyListeners();
  }

  void removeFromWishList({required ProductModel product}) {
    data.remove(product);
    notifyListeners();
  }



  void toggleFevoriteSingleProduct(int index) {
    data[index].isfavorite = !data[index].isfavorite;
    notifyListeners();
  }
}
