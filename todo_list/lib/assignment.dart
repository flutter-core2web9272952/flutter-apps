import 'package:flutter/material.dart';

class Assignment extends StatefulWidget {
  const Assignment({super.key});

  @override
  State<StatefulWidget> createState() {
    return _AssignmentState();
  }
}

class _AssignmentState extends State {
  TextEditingController _textController = TextEditingController();

  List<String> stringList = ["Sandesh"];

  int indx = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Assignment"),
        backgroundColor: Colors.blue,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 40,
            ),
            SizedBox(
              width: 400,
              child: TextField(
                onSubmitted: (data) {
                  stringList.add(_textController.text);
                  setState(() {});
                },
                controller: _textController,
                decoration: const InputDecoration(
                  hintText: "Enter Text",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(30))),
                ),
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            ListView.builder(
              shrinkWrap: true,
              itemCount: stringList.length,
              itemBuilder: (context, listIndex) {
                return Container(
                  width: 400,
                  margin: const EdgeInsets.all(10),
                  padding: const EdgeInsets.all(10),
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      color: Colors.grey,
                      boxShadow: const [
                        BoxShadow(
                            color: Colors.black,
                            offset: Offset(-5, 5),
                            blurRadius: 10)
                      ],
                      borderRadius: const BorderRadius.all(Radius.circular(10)),
                      border: Border.all(width: 1)),
                  child: Text(
                    stringList[listIndex],
                    style: const TextStyle(fontSize: 20, color: Colors.white),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.blue,
        onPressed: () {
          stringList.add(_textController.text);
          setState(() {});
        },
        child: const Icon(
          Icons.add,
        ),
      ),
    );
  }
}
