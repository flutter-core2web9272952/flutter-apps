import 'package:flutter/material.dart';

class Assignment10 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Assignment 8")]),
        actions: [
          const Icon(
            Icons.person_2,
          )
        ],
      ),
      body: Center(
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(
                width: 5,
                color: Colors.red,
              ),
              color: Colors.amber,
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20))),
          width: 300,
          height: 300,
        ),
      ),
    );
  }
}
