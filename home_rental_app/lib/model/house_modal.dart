class HouseModal {
  String imgUrl;
  String title;
  String location;
  String rent;
  String rating;
  int bedrooms;
  int bathrooms;
  String description;
  int squareFt;

  HouseModal({
    required this.imgUrl,
    required this.title,
    required this.location,
    required this.rent,
    required this.rating,
    required this.bathrooms,
    required this.bedrooms,
    required this.description,
    required this.squareFt,
  });
}
