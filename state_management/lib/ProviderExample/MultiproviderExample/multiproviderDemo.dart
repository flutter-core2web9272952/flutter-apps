import "dart:developer";

import "package:flutter/cupertino.dart";
import "package:flutter/material.dart";
import "package:provider/provider.dart";

class MyPage extends StatefulWidget {
  const MyPage({super.key});

  State createState() => _MyPageState();
}

class _MyPageState extends State {
  String name = "Ayush";
  @override
  Widget build(BuildContext context) {
    log("IN MAINAPP BUILD");

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("Multiprovider..."),
        centerTitle: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(Provider.of<Employee>(context).empName),
          const SizedBox(height: 30),
          Text("${Provider.of<Employee>(context).empId}"),
          const SizedBox(height: 30),
          Text(Provider.of<Project>(context).projectName),
          const SizedBox(height: 30),
          Text(Provider.of<Project>(context).devType),
          const SizedBox(height: 30),
          GestureDetector(
            onTap: () {
              Provider.of<Project>(context, listen: false)
                  .changeProject("Edtech", "Mobile dev");
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              decoration: BoxDecoration(color: Colors.blue),
              child: const Text("Change Text"),
            ),
          )
        ],
      ),
    );
  }
}

class Employee {
  String empName;
  int empId;

  Employee({required this.empName, required this.empId});
}

class Project with ChangeNotifier {
  String projectName;
  String devType;

  Project({required this.projectName, required this.devType});

  void changeProject(String projectName, String devType) {
    this.projectName = projectName;
    this.devType = devType;
    notifyListeners();
  }
}
