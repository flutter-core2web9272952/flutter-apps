import 'package:flutter/material.dart';
import 'package:state_management/Assignment1/models/product_model.dart';

class ProviderProductController with ChangeNotifier {
  List<ProductModel> data = [];
  ProductModel obj = ProductModel(
      productImg: "",
      productName: "",
      productPrice: "",
      isfavorite: false,
      quantity: 0);

  void changeProductInfo(
      {required String img, required String name, required String price}) {
    obj.productImg = img;
    obj.productName = name;
    obj.productPrice = price;

    notifyListeners();
  }

  void incrementQuantity() {
    obj.quantity += 1;
    notifyListeners();
  }

  void decrementQuantity() {
    obj.quantity -= 1;
    notifyListeners();
  }

  void toggleFevorite() {
    obj.isfavorite = !obj.isfavorite;
    notifyListeners();
  }

  void addProduct(
      {required String img, required String name, required String price}) {
    ProductModel obj = ProductModel(
      productImg: img,
      productName: name,
      productPrice: price,
      quantity: 0,
      isfavorite: false,
    );

    data.add(obj);
    notifyListeners();
  }

  void incrementQuantitySingleProduct(int index) {
    data[index].quantity += 1;
    notifyListeners();
  }

  void decrementQuantitySingleProduct(int index) {
    if (data[index].quantity > 0) data[index].quantity -= 1;
    notifyListeners();
  }

  void toggleFevoriteSingleProduct(int index) {
    data[index].isfavorite = !data[index].isfavorite;
    notifyListeners();
  }
}
