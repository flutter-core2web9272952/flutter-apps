import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MatchSummery extends StatefulWidget {
  const MatchSummery({super.key});

  @override
  State<StatefulWidget> createState() {
    return _MatchSummeryState();
  }
}

class _MatchSummeryState extends State {
  @override
  Widget build(BuildContext context) {
    log("IN MATCH Summery build");

    return Scaffold(
      appBar: AppBar(
        title: const Text("Consumer Demo"),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: Column(children: [
        Text(Provider.of<Player>(context).playerName),
        const SizedBox(
          height: 50,
        ),
        Text("${Provider.of<Player>(context).jerNo}"),
        const SizedBox(
          height: 50,
        ),
        Consumer(builder: (context, value, child) {
          log("In Consumer");
          return Column(
            children: [
              Text("${Provider.of<Match>(context).matchNo}"),
              const SizedBox(height: 50),
              Text("${Provider.of<Match>(context).runs}"),
            ],
          );
        }),
        const SizedBox(height: 30),
        ElevatedButton(
          onPressed: () {
            Provider.of<Match>(context, listen: false).changeData(250, 8900);
          },
          child: const Text("Change Data"),
        ),
        const SizedBox(height: 20),
        const NormalClass(),
      ]),
    );
  }
}

class NormalClass extends StatelessWidget {
  const NormalClass({super.key});

  @override
  Widget build(BuildContext context) {
    log("In NORMAL CLASS BUILD");
    return Consumer(builder: ((context, value, child) {
      return Text("${Provider.of<Match>(context).matchNo}");
    }));
  }
}

class Player {
  String playerName;
  int jerNo;

  Player({required this.playerName, required this.jerNo});
}

class Match extends ChangeNotifier {
  int matchNo;
  int runs;

  Match({required this.matchNo, required this.runs});

  void changeData(int matchNo, int runs) {
    this.matchNo = matchNo;
    this.runs = runs;
    notifyListeners();
  }
}
