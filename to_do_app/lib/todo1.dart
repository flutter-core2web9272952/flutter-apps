import 'package:flutter/material.dart';

class DisplayList extends StatefulWidget {
  const DisplayList({super.key});

  @override
  State<StatefulWidget> createState() => _DisplayList();
}

class _DisplayList extends State {
  int index = 0;

  List<String> list = [
    "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_640.jpg",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSK4JiH5eTM-BNubK-YJ6gcTPPJzuNn94HZmJM0LwMybHX-SnWUjlYQgRVPNhqcfTqY6s8&usqp=CAU",
    "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_640.jpg",
    "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_640.jpg",
    "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_640.jpg",
    "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_640.jpg",
    "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_640.jpg",
    "https://cdn.pixabay.com/photo/2015/04/19/08/32/marguerite-729510_640.jpg"
  ];

  List<String> list1 = [
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSK4JiH5eTM-BNubK-YJ6gcTPPJzuNn94HZmJM0LwMybHX-SnWUjlYQgRVPNhqcfTqY6s8&usqp=CAU",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: const Text("LISTVIEW"),
        ),
        body: ListView.builder(
            itemCount: list1.length,
            itemBuilder: (context, index) {
              return Container(
                  margin: const EdgeInsets.all(5),
                  width: double.infinity,
                  color: Colors.green,
                  alignment: Alignment.center,
                  child: Image.network(
                    list1[index],
                    width: 600,
                  ));
            }),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              list1.add(list[index]);
              index++;
            });
          },
          child: const Icon(Icons.add),
        ));
  }
}
