import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:state_management/Assignment1/controllers/product_controller.dart';
import 'package:state_management/Assignment1/controllers/wishlist_controller.dart';
import 'package:state_management/Assignment1/view/create_details.dart';
import 'package:state_management/ProviderExample/ConsumerExample/example1/consumer_example.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) {
            return ProviderProductController();
          },
        ),
        ChangeNotifierProvider(create: (context) {
          return ProviderWishlistController();
        })
      ],
      child: const MaterialApp(
        home: ProductDetails(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

/*

--------------------------------------------------------
Consumer is initialises... It is act as a seperate entity

@override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (context) {
            return Player(playerName: "MSD", jerNo: 7);
          },
        ),
        ChangeNotifierProvider(
          create: (context) {
            return Match(matchNo: 200, runs: 350);
          },
        ),
      ],
      child: MaterialApp(
        home: Scaffold(
          body: Consumer<Player>(
            builder: (context, value, child) {
              return Center(
                  child: Text(Provider.of<Player>(context).playerName));
            },
          ),
        ),
        debugShowCheckedModeBanner: false,
      ),
    );
  }

--------------------------------------------------------

in this we are using Provider before initialising


  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (context) {
            return Player(playerName: "MSD", jerNo: 7);
          },
        ),
        ChangeNotifierProvider(
          create: (context) {
            return Match(matchNo: 200, runs: 350);
          },
        ),
      ],
      child: MaterialApp(
        home: Scaffold(
          body: Center(
            child: Text(Provider.of<Player>(context).playerName),
          ),
        ),
        debugShowCheckedModeBanner: false,
      ),
    );
  }

---------------------------------------------------------
Consumer example1


  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (context) {
            return Player(playerName: "MSD", jerNo: 7);
          },
        ),
        ChangeNotifierProvider(
          create: (context) {
            return Match(matchNo: 200, runs: 350);
          },
        ),
      ],
      child: const MaterialApp(
        home: MatchSummery(),
        debugShowCheckedModeBanner: false,
      ),
    );
  }

----------------------------------------------------------

When we use Multiprovider

   @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (context) {
          return Employee(empName: "Sandesh", empId: 10);
        }),
        ChangeNotifierProvider(create: (context) {
          return Project(projectName: "GPU", devType: "System developer");
        })
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: true,
        home: MyPage(),
      ),
    );
  }


----------------------------------------------------------

When we use ChangeNotifier & ChangeNotifierProvider

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CompanyModal(compName: "Google", empCount: 1000),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Info(),
      ),
    );
  }

----------------------------------------------------------

Tried to chang data using normal provider uing its .value() constructor

  @override
  Widget build(BuildContext context) {
    return Provider.value(
      value: CompanyModal(compName: "Google", empCount: 1000),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Info(),
      ),
      updateShouldNotify: (previous, current) {
        print("In updateShouldNotify...");
        return previous.empCount != current.empCount;
      },
    );
  }

----------------------------------------------------------

When we use just Provider 

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (context) => CompanyModal(compName: "Google", empCount: 1000),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Info(),
      ),
    );
  }

----------------------------------------------------------


When we use InheritedWidget

    @override
  Widget build(BuildContext context) {
    return SharedData(
      data: Player(playerName: "", playerCountry: "", playerIplTeam: ""),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: FirstPage(),
      ),
    );
  }


 ---------------------------------------------------------

  */
