import 'package:api_demo/Assignment4/employee_controller.dart';
import 'package:flutter/material.dart';

import 'package:provider/provider.dart';

import 'Assignment4/home_page.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return EmployeeController();
      },
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
    );
  }
}
