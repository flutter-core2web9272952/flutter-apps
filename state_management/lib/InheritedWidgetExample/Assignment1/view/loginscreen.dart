import 'package:flutter/material.dart';
import 'package:state_management/InheritedWidgetExample/Assignment1/model.dart';
import 'package:state_management/InheritedWidgetExample/Assignment1/sharedData.dart';
import 'package:state_management/InheritedWidgetExample/Assignment1/view/infoscreen.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State {
  final TextEditingController _idController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _userNameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SharedData sharedObject = SharedData.of(context);

    return Scaffold(
      body: Column(
        children: [
          const SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 300,
                height: 50,
                child: TextFormField(
                  controller: _idController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Id",
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 300,
                height: 50,
                child: TextFormField(
                  controller: _nameController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Name",
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 300,
                height: 50,
                child: TextFormField(
                  controller: _userNameController,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    hintText: "Username",
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
            ],
          ),
          const SizedBox(
            height: 100,
          ),
          GestureDetector(
            onTap: () {
              sharedObject.data.id = _idController.text;
              sharedObject.data.name = _nameController.text;
              sharedObject.data.userName = _userNameController.text;

              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const InfoPage()));
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              decoration: BoxDecoration(
                color: Colors.purple.shade100,
                borderRadius: BorderRadius.circular(12),
              ),
              child: const Text(
                "Submit",
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
