import 'package:container_app/assignment1.dart';
import 'package:container_app/assignment2.dart';
import 'package:container_app/assignment3.dart';
import 'package:container_app/assignment4.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Assignment4(),
    );
  }
}
