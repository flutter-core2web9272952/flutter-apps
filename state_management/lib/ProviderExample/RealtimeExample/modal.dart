class Team {
  String logoString;
  String teamName;
  int matches;
  int wins;
  int losses;

  Team({
    required this.logoString,
    required this.teamName,
    required this.matches,
    required this.wins,
    required this.losses,
  });

  
}
