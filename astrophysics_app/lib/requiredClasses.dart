class SingleModalObject {
  String name;
  String img;
  String info;
  String moreInfo;
  String bigImg;
  String websiteLink;

  SingleModalObject(
      {required this.name,
      required this.img,
      required this.info,
      required this.moreInfo,
      required this.websiteLink,
      required this.bigImg});
}

class SingleModalPlanet {
  int? id;
  String planetName;
  String planetImg;
  String info;
  String moreInfo;
  String bigImg;
  String websiteLink;

  SingleModalPlanet({
    required this.planetName,
    required this.planetImg,
    required this.info,
    this.id,
    required this.moreInfo,
    required this.bigImg,
    required this.websiteLink,
  });

  Map<String, dynamic> getPlanetMap() {
    return {
      "planetName": planetName,
      "planetImg": planetImg,
      "info": info,
      "moreInfo": moreInfo,
      "bigImg": bigImg,
      "websiteLink": websiteLink,
    };
  }

  @override
  String toString() {
    return "id $id $planetName planetImg:$planetImg \n info:$info";
  }
}

class SingleModalAsteroid {
  int? id;
  String asteroidName;
  String asteroidImg;
  String info;
  String moreInfo;
  String bigImg;
  String websiteLink;

  SingleModalAsteroid({
    required this.asteroidName,
    required this.asteroidImg,
    required this.info,
    this.id,
    required this.moreInfo,
    required this.bigImg,
    required this.websiteLink,
  });

  Map<String, dynamic> getAsteroidMap() {
    return {
      "asteroidName": asteroidName,
      "asteroidImg": asteroidImg,
      "info": info,
      "moreInfo": moreInfo,
      "bigImg": bigImg,
      "websiteLink": websiteLink,
    };
  }

  @override
  String toString() {
    return "asteroidName:$asteroidName asteroidImg:$asteroidImg \n info:$info";
  }
}

class SingleModalGalaxy {
  int? id;
  String galaxyName;
  String galaxyImg;
  String info;
  String moreInfo;
  String bigImg;
  String websiteLink;

  SingleModalGalaxy({
    required this.galaxyName,
    required this.galaxyImg,
    required this.info,
    this.id,
    required this.moreInfo,
    required this.bigImg,
    required this.websiteLink,
  });

  Map<String, dynamic> getGalaxyMap() {
    return {
      "galaxyName": galaxyName,
      "galaxyImg": galaxyImg,
      "info": info,
      "moreInfo": moreInfo,
      "bigImg": bigImg,
      "websiteLink": websiteLink,
    };
  }

  @override
  String toString() {
    return "galaxyName:$galaxyName galaxyImg:$galaxyImg \n info:$info";
  }
}

class SingleModalOthers {
  int? id;
  String objectName;
  String objectImg;
  String info;
  String moreInfo;
  String bigImg;
  String websiteLink;

  SingleModalOthers({
    required this.objectName,
    required this.objectImg,
    required this.info,
    this.id,
    required this.moreInfo,
    required this.bigImg,
    required this.websiteLink,
  });

  Map<String, dynamic> getOtherMap() {
    return {
      "objectName": objectName,
      "objectImg": objectImg,
      "info": info,
      "moreInfo": moreInfo,
      "bigImg": bigImg,
      "websiteLink": websiteLink,
    };
  }

  @override
  String toString() {
    return "objectName:$objectName objectImg:$objectImg \n info:$info";
  }
}

class SingleModalUserData {
  int? id;
  String name;
  String userName;
  String password;

  SingleModalUserData(
      {required this.name,
      required this.userName,
      required this.password,
      this.id});

  Map<String, dynamic> getUserMap() {
    return {
      "name": name,
      "userName": userName,
      "password": password,
    };
  }
}
