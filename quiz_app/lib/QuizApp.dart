import 'package:flutter/material.dart';

class QuizApp extends StatefulWidget {
  const QuizApp({super.key});

  @override
  State<StatefulWidget> createState() {
    return _QuizAppState();
  }
}

class Widget1 extends StatefulWidget {
  const Widget1({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Widget1State();
  }
}

class _Widget1State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold();
  }
}

class _QuizAppState extends State<QuizApp> {
  void initState() {
    super.initState();
    print("inside initState");
  }

  @override
  void dispose() {
    print("inside dispose");
    super.dispose();
  }

  void didChangeDependencies() {
    super.didChangeDependencies();
    print("inside didChangedDependencies");
  }

  @override
  void deactivate() {
    print("inside deactivate");
    super.deactivate();
  }

  @override
  void didUpdateWidget(QuizApp oldWidget) {
    super.didUpdateWidget(oldWidget);
    print("In didChangedWidget");
  }

  List<Map> allQuestions = [
    {
      "question": "Who is founder of Microsoft ? ",
      "options": ["Steve Jobs", "Bill Gates", "Jeff Bezos", "Elon Musk"],
      "answerIndex": 1
    },
    {
      "question": "Who is founder of Tesla ? ",
      "options": ["Steve Jobs", "Bill Gates", "Jeff Bezos", "Elon Musk"],
      "answerIndex": 3
    },
    {
      "question": "Who is founder of Amazon ? ",
      "options": ["Steve Jobs", "Bill Gates", "Jeff Bezos", "Elon Musk"],
      "answerIndex": 2
    },
    {
      "question": "Who is founder of Apple ? ",
      "options": ["Steve Jobs", "Bill Gates", "Jeff Bezos", "Elon Musk"],
      "answerIndex": 0
    }
  ];

  bool questionScreen = true;
  int questionIndex = 0;

  // to store is current btn selection is right or not
  int isRight1 = -1;
  int isRight2 = -1;
  int isRight3 = -1;
  int isRight4 = -1;

  // to store points if answer is right
  int score = 0;

  // to store is option is selected once
  bool isSelected = false;

  Scaffold isQuestionScreen() {
    if (questionScreen == true) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          centerTitle: true,
          title: const Text(
            "QuizApp",
            style: TextStyle(
              fontSize: 30,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
        body: Column(
          children: [
            const SizedBox(
              height: 25,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  "Questions : ",
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600),
                ),
                Text(
                  "${questionIndex + 1} / ${allQuestions.length}",
                  style: const TextStyle(
                      fontSize: 25, fontWeight: FontWeight.w600),
                )
              ],
            ),
            const SizedBox(
              height: 50,
            ),
            SizedBox(
              width: 380,
              height: 50,
              child: Text(
                textAlign: TextAlign.center,
                "${allQuestions[questionIndex]["question"]}",
                style:
                    const TextStyle(fontSize: 23, fontWeight: FontWeight.w400),
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  isRight1 == 1
                      ? Colors.green
                      : isRight1 == 0
                          ? Colors.red
                          : null,
                ),
              ),
              onPressed: () {
                setState(() {
                  if (!isSelected) {
                    if (allQuestions[questionIndex]["answerIndex"] == 0) {
                      isRight1 = 1;
                      score++;
                    } else {
                      isRight1 = 0;
                    }
                  }

                  isSelected = true;
                });
              },
              child: Text(
                "A.${allQuestions[questionIndex]["options"][0]}",
                style: const TextStyle(
                    fontSize: 20, fontWeight: FontWeight.normal),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  isRight2 == 1
                      ? Colors.green
                      : isRight2 == 0
                          ? Colors.red
                          : null,
                ),
              ),
              onPressed: () {
                setState(() {
                  if (!isSelected) {
                    if (allQuestions[questionIndex]["answerIndex"] == 1) {
                      isRight2 = 1;
                      score++;
                    } else {
                      isRight2 = 0;
                    }
                  }

                  isSelected = true;
                });
              },
              child: Text(
                "B.${allQuestions[questionIndex]["options"][1]}",
                style: const TextStyle(
                    fontSize: 20, fontWeight: FontWeight.normal),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  isRight3 == 1
                      ? Colors.green
                      : isRight3 == 0
                          ? Colors.red
                          : null,
                ),
              ),
              onPressed: () {
                setState(() {
                  if (!isSelected) {
                    if (allQuestions[questionIndex]["answerIndex"] == 2) {
                      isRight3 = 1;
                      score++;
                    } else {
                      isRight3 = 0;
                    }
                  }
                  isSelected = true;
                });
              },
              child: Text(
                "C.${allQuestions[questionIndex]["options"][2]}",
                style: const TextStyle(
                    fontSize: 20, fontWeight: FontWeight.normal),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all(
                  isRight4 == 1
                      ? Colors.green
                      : isRight4 == 0
                          ? Colors.red
                          : null,
                ),
              ),
              onPressed: () {
                setState(() {
                  if (!isSelected) {
                    if (allQuestions[questionIndex]["answerIndex"] == 3) {
                      isRight4 = 1;
                      score++;
                    } else {
                      isRight4 = 0;
                    }
                  }

                  isSelected = true;
                });
              },
              child: Text(
                "D.${allQuestions[questionIndex]["options"][3]}",
                style: const TextStyle(
                    fontSize: 20, fontWeight: FontWeight.normal),
              ),
            ),
            const SizedBox(
              height: 20,
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            if (isSelected) {
              isRight1 = -1;
              isRight2 = -1;
              isRight3 = -1;
              isRight4 = -1;

              isSelected = false;
              questionIndex++;
            }

            if (questionIndex > allQuestions.length - 1) {
              questionScreen = false;
            }

            setState(() {});
          },
          child: Icon(Icons.arrow_forward_sharp),
        ),
      );
    } else {
      return Scaffold(
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.network(
                  "https://i.pinimg.com/736x/82/51/ca/8251ca07297c8122c91acc3fc8af5ada.jpg",
                  width: 200,
                ),
                const SizedBox(
                  height: 30,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      "Your Score:  ",
                      style: TextStyle(
                          color: Color.fromARGB(255, 0, 0, 0), fontSize: 35),
                    ),
                    Text(
                      "$score / ${allQuestions.length} ",
                      style: const TextStyle(
                          color: Color.fromARGB(255, 250, 192, 2),
                          fontSize: 35),
                    )
                  ],
                ),
              ],
            )),
            const SizedBox(
              height: 50,
            ),
            ElevatedButton(
                onPressed: () {
                  isRight1 = -1;
                  isRight2 = -1;
                  isRight3 = -1;
                  isRight4 = -1;

                  isSelected = false;
                  questionIndex = 0;
                  questionScreen = true;

                  score = 0;

                  setState(() {});
                },
                child: const Text("Restart")),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return isQuestionScreen();
  }
}
