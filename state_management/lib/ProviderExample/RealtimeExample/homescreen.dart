import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:state_management/ProviderExample/RealtimeExample/components.dart';
import 'package:state_management/ProviderExample/RealtimeExample/modal.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  State createState() => _HomePageState();
}

class _HomePageState extends State {
  List<Team> teams = [
    Team(
        logoString: "assets/ChennaiSuperKings.svg",
        teamName: "CSK",
        matches: 0,
        wins: 0,
        losses: 0),
    Team(
        logoString: "assets/DelhiCapitals.svg",
        teamName: "DC",
        matches: 0,
        wins: 0,
        losses: 0),
    Team(
        logoString: "assets/GujaratTitans.svg",
        teamName: "GT",
        matches: 0,
        wins: 0,
        losses: 0),
    Team(
        logoString: "assets/KolkataKnightRiders.svg",
        teamName: "KKR",
        matches: 0,
        wins: 0,
        losses: 0),
    Team(
        logoString: "assets/LucknowSuperGiants.svg",
        teamName: "LSG",
        matches: 0,
        wins: 0,
        losses: 0),
    Team(
        logoString: "assets/MumbaiIndians.svg",
        teamName: "MI",
        matches: 0,
        wins: 0,
        losses: 0),
    Team(
        logoString: "assets/PunjabKings.svg",
        teamName: "PBKS",
        matches: 0,
        wins: 0,
        losses: 0),
    Team(
        logoString: "assets/RajasthanRoyals.svg",
        teamName: "RR",
        matches: 0,
        wins: 0,
        losses: 0),
    Team(
        logoString: "assets/RoyalChallengersBangalore.svg",
        teamName: "RCB",
        matches: 0,
        wins: 0,
        losses: 0),
    Team(
        logoString: "assets/SunrisersHyderabad.svg",
        teamName: "SRH",
        matches: 0,
        wins: 0,
        losses: 0),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Points Table",
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      drawer: MyDrawer(),
      body: Table(
        columnWidths: const {
          0: FlexColumnWidth(3),
          1: FlexColumnWidth(1),
          2: FlexColumnWidth(1),
          3: FlexColumnWidth(1),
        },
        border: const TableBorder(
            bottom: BorderSide(color: Color.fromARGB(38, 0, 0, 0))),
        children: [
          const TableRow(children: [
            TableCell(
              child: Center(
                child: Text(
                  "Team",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            TableCell(
              child: Center(
                child: Text(
                  "M",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            TableCell(
              child: Center(
                child: Text(
                  "W",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
            TableCell(
              child: Center(
                child: Text(
                  "L",
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
            ),
          ]),
          ...generateTableRow()
        ],
      ),
    );
  }

  //...generateTableRow()

  List<TableRow> generateTableRow() {
    return teams.map((data) {
      return TableRow(
        decoration: const BoxDecoration(
            border:
                Border(top: BorderSide(color: Color.fromARGB(45, 0, 0, 0)))),
        children: [
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.middle,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  data.logoString,
                  width: 50,
                  height: 50,
                ),
                const SizedBox(
                  width: 20,
                ),
                Text(
                  data.teamName,
                  style: const TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w600,
                  ),
                )
              ],
            ),
          ),
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.middle,
            child: Center(
                child: Text(
              "${data.matches}",
            )),
          ),
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.middle,
            child: Center(child: Text("${data.wins}")),
          ),
          TableCell(
            verticalAlignment: TableCellVerticalAlignment.middle,
            child: Center(child: Text("${data.losses}")),
          ),
        ],
      );
    }).toList();
  }
}
