import 'package:flutter/material.dart';
import 'package:instagram/profilePage.dart';
import 'package:instagram/reelsPage.dart';
import 'package:instagram/test.dart';
import 'searchpage.dart';
import 'homePage.dart';

class Instagram extends StatefulWidget {
  const Instagram({super.key});
  @override
  State<Instagram> createState() => _MainPage();
}

class _MainPage extends State<Instagram> {
  int _curIndex = 0;

  final List<Widget> pages = [
    const HomePage(),
    const SearchPage(),
    ReelsPage(),
    const SearchPage(),
    const Profile(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[_curIndex],
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _curIndex,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          fixedColor: Colors.black,
          unselectedItemColor: const Color.fromARGB(207, 0, 0, 0),
          onTap: (index) => {
                setState(() {
                  _curIndex = index;
                })
              },
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              activeIcon: Icon(Icons.home_sharp),
              label: "home",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.search_outlined),
              activeIcon: Icon(Icons.search_sharp),
              label: "Search",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.add_box_outlined),
              activeIcon: Icon(Icons.add_box_sharp),
              label: "Add",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite_border_outlined),
              activeIcon: Icon(Icons.favorite_sharp),
              label: "Notifications",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person_outlined),
              activeIcon: Icon(Icons.person_sharp),
              label: "Profile",
            ),
          ]),
    );
  }
}



/*


*/