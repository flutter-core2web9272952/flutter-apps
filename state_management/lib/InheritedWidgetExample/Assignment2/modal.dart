class Player {
  String playerName;
  String playerCountry;
  String playerIplTeam;

  Player({
    required this.playerName,
    required this.playerCountry,
    required this.playerIplTeam,
  });

  String toString() {
    return "$playerName  $playerCountry  $playerIplTeam";
  }
}
