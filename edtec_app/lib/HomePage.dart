import 'package:edtec_app/CoursesScreen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(205, 218, 218, 1),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    IconButton(
                      onPressed: () {},
                      icon: const Icon(Icons.menu),
                    ),
                    Spacer(),
                    const Icon(Icons.notifications),
                  ],
                ),
                Text(
                  "Welcome to",
                  style: GoogleFonts.jost(
                      fontSize: 26.98, fontWeight: FontWeight.w300),
                ),
                Text(
                  "Educourse",
                  style: GoogleFonts.jost(
                      fontSize: 37, fontWeight: FontWeight.w700),
                ),
                const SizedBox(
                  height: 15,
                ),
                TextField(
                  decoration: InputDecoration(
                      contentPadding:
                          const EdgeInsets.symmetric(horizontal: 20),
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: GestureDetector(
                            child: const Icon(
                              Icons.search,
                              color: Colors.black,
                            ),
                          )),
                      hintText: "Enter your keyword",
                      hintStyle: const TextStyle(
                          color: Color.fromRGBO(143, 143, 143, 1),
                          fontSize: 14,
                          fontWeight: FontWeight.w400),
                      border: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(28.5)),
                        borderSide: BorderSide(color: Colors.transparent),
                      ),
                      focusedBorder: const OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(28.5)),
                          borderSide: BorderSide(color: Colors.transparent))),
                ),
              ],
            ),
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                width: double.infinity,
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(38),
                        topRight: Radius.circular(38))),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Course For You",
                      style: GoogleFonts.jost(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          Container(
                            padding: const EdgeInsets.all(15),
                            height: 242,
                            width: 190,
                            decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14)),
                                gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      Color.fromRGBO(197, 4, 98, 1),
                                      Color.fromRGBO(80, 3, 112, 1)
                                    ])),
                            child: Column(
                              children: [
                                Text(
                                  "UX Designer from Scratch.",
                                  style: GoogleFonts.jost(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 17,
                                      color: Colors.white),
                                ),
                                SvgPicture.asset(
                                  "assets/cardImages/img1.svg",
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          Container(
                            padding: const EdgeInsets.all(15),
                            height: 242,
                            width: 190,
                            decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(14)),
                                gradient: LinearGradient(
                                    begin: Alignment.topCenter,
                                    end: Alignment.bottomCenter,
                                    colors: [
                                      Color.fromRGBO(0, 77, 228, 1),
                                      Color.fromRGBO(1, 47, 135, 1)
                                    ])),
                            child: Column(
                              children: [
                                Text(
                                  "Design Thinking The Beginner",
                                  style: GoogleFonts.jost(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 17,
                                      color: Colors.white),
                                ),
                                SvgPicture.asset(
                                  "assets/cardImages/img2.svg",
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Text(
                      "Course By Category",
                      style: GoogleFonts.jost(
                          fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.push(context,
                                    MaterialPageRoute(builder: (context) {
                                  return const CoursesScreen();
                                }));
                              },
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                width: 36,
                                height: 36,
                                decoration: const BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(8)),
                                  color: Color.fromRGBO(25, 0, 56, 1),
                                ),
                                child: SvgPicture.asset(
                                  "assets/svgIcons/icon1.svg",
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 9,
                            ),
                            Text(
                              "UI/UX",
                              style: GoogleFonts.jost(
                                  fontSize: 14, fontWeight: FontWeight.w400),
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(8),
                              width: 36,
                              height: 36,
                              decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                color: Color.fromRGBO(25, 0, 56, 1),
                              ),
                              child: SvgPicture.asset(
                                "assets/svgIcons/icon2.svg",
                              ),
                            ),
                            const SizedBox(
                              height: 9,
                            ),
                            Text(
                              "Visual",
                              style: GoogleFonts.jost(
                                  fontSize: 14, fontWeight: FontWeight.w400),
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(8),
                              width: 36,
                              height: 36,
                              decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                color: Color.fromRGBO(25, 0, 56, 1),
                              ),
                              child: SvgPicture.asset(
                                "assets/svgIcons/icon3.svg",
                              ),
                            ),
                            const SizedBox(
                              height: 9,
                            ),
                            Text(
                              "Illusrtration",
                              style: GoogleFonts.jost(
                                  fontSize: 14, fontWeight: FontWeight.w400),
                            )
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(8),
                              width: 36,
                              height: 36,
                              decoration: const BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(8)),
                                color: Color.fromRGBO(25, 0, 56, 1),
                              ),
                              child: SvgPicture.asset(
                                "assets/svgIcons/icon4.svg",
                              ),
                            ),
                            const SizedBox(
                              height: 9,
                            ),
                            Text(
                              "Photo",
                              style: GoogleFonts.jost(
                                  fontSize: 14, fontWeight: FontWeight.w400),
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
