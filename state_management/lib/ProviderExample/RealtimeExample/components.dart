import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:state_management/ProviderExample/RealtimeExample/homescreen.dart';

class MyDrawer extends StatefulWidget {
  const MyDrawer({super.key});

  @override
  State<StatefulWidget> createState() {
    return _MyDrawerState();
  }
}

class _MyDrawerState extends State {
  int pageIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      width: 250,
      child: Column(
        children: [
          const SizedBox(
            height: 50,
          ),
          const Text(
            "Indian Premier League",
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.w600,
            ),
          ),
          const Divider(),
          const SizedBox(
            height: 20,
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const HomePage(),
                ),
              );
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                  border: const Border(bottom: BorderSide(color: Colors.white)),
                  color: pageIndex == 0
                      ? Color.fromARGB(185, 15, 15, 20)
                      : Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Points Table",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: pageIndex == 0
                            ? const Color.fromARGB(255, 255, 255, 255)
                            : Colors.white),
                  ),
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (context) => const HomePage(),
                ),
              );
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              decoration: BoxDecoration(
                  color: pageIndex == 0
                      ? const Color.fromARGB(185, 15, 15, 20)
                      : Colors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Play Match",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: pageIndex == 0
                            ? const Color.fromARGB(255, 255, 255, 255)
                            : Colors.white),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
