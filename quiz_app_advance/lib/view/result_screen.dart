import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pie_chart/pie_chart.dart';

class ResultPage extends StatefulWidget {
  const ResultPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ResultPageState();
  }
}

class _ResultPageState extends State {
  Map<String, double> data = {"total": 68};

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            "assets/bg.jpg",
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          Positioned(
            top: 51,
            left: 0,
            right: 0,
            child: Container(
              child: Column(
                children: [
                  Text(
                    "Quiz Result",
                    style: GoogleFonts.dmSans(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: const Color.fromRGBO(131, 76, 52, 1)),
                  ),
                  Text(
                    "Math",
                    style: GoogleFonts.dmSans(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(131, 76, 52, 1)),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height / 2 - 130,
            left: 0,
            right: 0,
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 13),
              decoration: BoxDecoration(
                color: const Color.fromRGBO(246, 221, 195, 1),
                borderRadius: BorderRadius.circular(15),
                boxShadow: const [
                  BoxShadow(
                    offset: Offset(0, 0),
                    blurRadius: 8,
                    spreadRadius: 0,
                    color: Color.fromRGBO(0, 0, 0, 0.1),
                  )
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 17,
                  vertical: 31,
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const SizedBox(height: 35),
                    Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 17,
                        vertical: 20,
                      ),
                      decoration: BoxDecoration(
                          color: const Color.fromRGBO(248, 145, 87, 1),
                          borderRadius: BorderRadius.circular(15)),
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Total won quiz's till now",
                                  style: GoogleFonts.dmSans(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w700,
                                      color: const Color.fromRGBO(0, 0, 0, 1)),
                                ),
                                const SizedBox(height: 5),
                                Text(
                                  "Lorem Ipsum has been the industry's standard dummy scrambled it to make a type specimen book.",
                                  style: GoogleFonts.dmSans(
                                      fontSize: 10,
                                      fontWeight: FontWeight.w400,
                                      color: const Color.fromRGBO(0, 0, 0, 1)),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(
                            width: 23,
                          ),
                          PieChart(
                            animationDuration:
                                const Duration(milliseconds: 2000),
                            chartType: ChartType.ring,
                            dataMap: data,
                            colorList: [Colors.white],
                            chartRadius: 50,
                            ringStrokeWidth: 10,
                            centerWidget: Text(
                              "${data["total"]}%",
                              style: GoogleFonts.dmSans(
                                  fontSize: 10,
                                  fontWeight: FontWeight.w400,
                                  color: const Color.fromRGBO(0, 0, 0, 1)),
                            ),
                            chartValuesOptions: const ChartValuesOptions(
                                showChartValues: false),
                            legendOptions: LegendOptions(showLegends: false),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 19),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Flexible(
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 30),
                            decoration: BoxDecoration(
                              color: const Color.fromRGBO(250, 245, 241, 1),
                              borderRadius: BorderRadius.circular(15),
                              boxShadow: const [
                                BoxShadow(
                                  offset: Offset(0, 0),
                                  blurRadius: 8,
                                  spreadRadius: 0,
                                  color: Color.fromRGBO(0, 0, 0, 0.1),
                                )
                              ],
                            ),
                            child: Column(
                              children: [
                                Text(
                                  "Solved Questions",
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.dmSans(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                      color: const Color.fromRGBO(
                                          131, 76, 52, 0.9)),
                                ),
                                const SizedBox(height: 6),
                                Text(
                                  "20",
                                  style: GoogleFonts.dmSans(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700,
                                      color:
                                          const Color.fromRGBO(131, 76, 52, 1)),
                                ),
                              ],
                            ),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Flexible(
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                                vertical: 18, horizontal: 30),
                            decoration: BoxDecoration(
                                color: const Color.fromRGBO(26, 181, 134, 1),
                                borderRadius: BorderRadius.circular(15),
                                boxShadow: const [
                                  BoxShadow(
                                    offset: Offset(0, 0),
                                    blurRadius: 8,
                                    spreadRadius: 0,
                                    color: Color.fromRGBO(0, 0, 0, 0.1),
                                  )
                                ]),
                            child: Column(
                              children: [
                                Text(
                                  "Correct Questions",
                                  textAlign: TextAlign.center,
                                  style: GoogleFonts.dmSans(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w700,
                                      color: const Color.fromRGBO(
                                          255, 255, 255, 0.87)),
                                ),
                                const SizedBox(height: 6),
                                Text(
                                  "20",
                                  style: GoogleFonts.dmSans(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w700,
                                      color: const Color.fromRGBO(
                                          255, 255, 255, 1)),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Positioned(
            top: MediaQuery.of(context).size.height / 2 - 170,
            left: MediaQuery.of(context).size.width / 2 - 40,
            child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      offset: Offset(0, 0),
                      blurRadius: 10,
                      spreadRadius: 0,
                      color: Color.fromRGBO(0, 0, 0, 0.1),
                    )
                  ],
                ),
                child: Text(
                  "M",
                  style: GoogleFonts.dmSans(
                      fontSize: 40,
                      fontWeight: FontWeight.w700,
                      color: const Color.fromRGBO(200, 60, 0, 1)),
                )),
          ),
        ],
      ),
    );
  }
}
