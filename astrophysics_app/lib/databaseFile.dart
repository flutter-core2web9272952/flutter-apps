import 'package:astrophysics_app/requiredClasses.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

import 'dart:io';

dynamic database;

//this is a function which returns a instance of database which contains table related to user data
Future<dynamic> myCreateDB() async {
  if (Platform.isLinux) {
    databaseFactory = databaseFactoryFfi;
    return await databaseFactory.openDatabase(
        p.join(await getDatabasesPath(), "UserDataBase.db"),
        options: OpenDatabaseOptions(
          version: 1,
          onCreate: (db, version) {
            db.execute(''' CREATE TABLE UserData(
                id INTEGER PRIMARY KEY,
                name TEXT,
                userName TEXT,
                password TEXT
                )
              ''');
          },
        ));
  } else {
    // this syntax is to create database on mobile applications
    return openDatabase(
      p.join(await getDatabasesPath(), "UserDataBase.db"),
      version: 1,
      onCreate: (db, version) {
        db.execute(''' CREATE TABLE UserData (
                id INTEGER PRIMARY KEY,
                name TEXT,
                userName TEXT,
                password TEXT
                )
              ''');
      },
    );
  }
}

// funtion to insert data in userData table
Future<void> insertUserData(SingleModalUserData obj) async {
  final localDB = await database;

  localDB.insert(
    "UserData",
    obj.getUserMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

// funtion to fetch data from userData table
Future<List<SingleModalUserData>> fetchUserData() async {
  final localDb = await database;

  List<Map<String, dynamic>> mapEntry = await localDb.query(
    "UserData",
  );

  return List.generate(mapEntry.length, (i) {
    return SingleModalUserData(
      id: mapEntry[i]["id"],
      name: mapEntry[i]["name"],
      userName: mapEntry[i]["userName"],
      password: mapEntry[i]["password"],
    );
  });
}

//this is a function ,returns a instance of database which contains table related to Planet,Asteroid,Galaxy and Other Object
Future<dynamic> mycreateDatabaseFunction() async {
  if (Platform.isLinux) {
    databaseFactory = databaseFactoryFfi;
    return await databaseFactory.openDatabase(
        p.join(await getDatabasesPath(), "Astrophysics.db"),
        options: OpenDatabaseOptions(
          version: 1,
          onCreate: (db, version) {
            db.execute(''' CREATE TABLE Planets (
                id INTEGER PRIMARY KEY,
                planetName TEXT,
                planetImg TEXT,
                info TEXT,
                moreInfo TEXT,
                bigImg TEXT,
                websiteLink TEXT
                )
              ''');

            db.execute(''' CREATE TABLE Galaxies (
                id INTEGER PRIMARY KEY,
                galaxyName TEXT,
                galaxyImg TEXT,
                info TEXT,
                moreInfo TEXT,
                bigImg TEXT,
                websiteLink TEXT
                )
              ''');

            db.execute(''' CREATE TABLE Asteroids (
                id INTEGER PRIMARY KEY,
                asteroidName TEXT,
                asteroidImg TEXT,
                info TEXT,
                moreInfo TEXT,
                bigImg TEXT,
                websiteLink TEXT
                )
              ''');

            db.execute(''' CREATE TABLE Others (
                id INTEGER PRIMARY KEY,
                objectName TEXT,
                objectImg TEXT,
                info TEXT,
                moreInfo TEXT,
                bigImg TEXT,
                websiteLink TEXT
                )
              ''');
          },
        ));
  } else {
    // this syntax is to create database on mobile applications
    return openDatabase(
      p.join(await getDatabasesPath(), "Astrophysics.db"),
      version: 1,
      onCreate: (db, version) {
        db.execute(''' CREATE TABLE Planets (
                id INTEGER PRIMARY KEY,
                planetName TEXT,
                planetImg TEXT,
                info TEXT,
                moreInfo TEXT,
                bigImg TEXT,
                websiteLink TEXT
                )
              ''');

        db.execute(''' CREATE TABLE Galaxies (
                id INTEGER PRIMARY KEY,
                galaxyName TEXT,
                galaxyImg TEXT,
                info TEXT,
                moreInfo TEXT,
                bigImg TEXT,
                websiteLink TEXT
                )
              ''');

        db.execute(''' CREATE TABLE Asteroids (
                id INTEGER PRIMARY KEY,
                asteroidName TEXT,
                asteroidImg TEXT,
                info TEXT,
                moreInfo TEXT,
                bigImg TEXT,
                websiteLink TEXT
                )
              ''');

        db.execute(''' CREATE TABLE Others (
                id INTEGER PRIMARY KEY,
                objectName TEXT,
                objectImg TEXT,
                info TEXT,
                moreInfo TEXT,
                bigImg TEXT,
                websiteLink TEXT
                )
              ''');
      },
    );
  }
}
