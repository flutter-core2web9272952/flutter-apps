import 'package:flutter/material.dart';

class Assignment7 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.deepPurple,
        title: const Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [Text("Assignment 7")]),
        actions: [Icon(Icons.qr_code_2_outlined)],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Container(
          padding: const EdgeInsets.symmetric(),
          child: Row(
            children: [
              Image.network(
                "https://img.freepik.com/free-photo/painting-mountain-lake-with-mountain-background_188544-9126.jpg",
                width: 150,
                height: 300,
              ),
              const SizedBox(width: 10),
              Image.network(
                "https://i.pinimg.com/736x/43/ea/98/43ea98154d1587502b02722b7924ca27.jpg",
                width: 150,
                height: 300,
              ),
              const SizedBox(width: 10),
              Image.network(
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9dBoZbsAB09tT0YWTUj1cPPV8YP4w22ha1uuRHbzrqz29nKZOGCjMKeVIFe37V1m9ukM&usqp=CAU",
                width: 150,
                height: 300,
              ),
              const SizedBox(width: 10),
              Image.network(
                "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSWW3G-NhOB1SF2USmQfBm4y0r3_8rYni5xV4FgFrg1AnIUdkGF57yx8theFI4dH-sAhIw&usqp=CAU",
                width: 150,
                height: 300,
              ),
              const SizedBox(width: 10),
              Image.network(
                "https://www.lightstalking.com/wp-content/uploads/backlit-beach-color-258109-3-1024x576.jpg",
                width: 150,
                height: 300,
              )
            ],
          ),
        ),
      ),
    );
  }
}
