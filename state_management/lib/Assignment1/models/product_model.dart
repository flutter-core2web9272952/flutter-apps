class ProductModel {
  String productName;
  String productImg;
  String productPrice;
  bool isfavorite;
  int quantity;

  ProductModel({
    required this.productImg,
    required this.productName,
    required this.productPrice,
    required this.isfavorite,
    required this.quantity,
  });
}
