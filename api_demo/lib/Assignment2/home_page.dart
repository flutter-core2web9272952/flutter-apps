import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:developer';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State {
  Map<String, dynamic> empData = {};

  Widget showData() {
    log("In showData1");

    if (empData.isNotEmpty) {
      log("In showData2");

      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(empData['employee_name']),
          const SizedBox(
            width: 30,
          ),
          Text("${empData['employee_salary']}")
        ],
      );
    } else {
      return const Center(child: Text("No Data"));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: const Text("API Binding"),
      ),
      body: showData(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          getEmployeeData();
        },
        child: const Icon(Icons.add),
      ),
    );
  }

  void getEmployeeData() async {
    Uri url = Uri.parse("https://dummy.restapiexample.com/api/v1/employee/1");
    http.Response response = await http.get(url);
    var responseData = json.decode(response.body);

    setState(() {
      empData = responseData['data'];
    });

    log("${empData}");
  }
}


/*

  Main code

  class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: HomeScreen(),
    );
  }
}


*/