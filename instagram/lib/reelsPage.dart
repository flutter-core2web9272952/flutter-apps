import 'package:flutter/material.dart';

class ReelsPage extends StatefulWidget {
  const ReelsPage({super.key});

  @override
  State<ReelsPage> createState() {
    return _ReelsPageState();
  }
}

class _ReelsPageState extends State<ReelsPage> {
  List<List<String>> reelData = [
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://images.unsplash.com/photo-1570498839593-e565b39455fc?q=80&w=1935&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D"
    ],
    [
      "virat.kohli",
      "https://imagevars.gulfnews.com/2022/09/12/Anushka-Virat1_18333434bb6_original-ratio.jpg",
      "https://i.pinimg.com/564x/b0/87/3a/b0873a57b79124492a76141fd857f2d4.jpg",
    ],
    [
      "virat.kohli",
      "https://imagevars.gulfnews.com/2022/09/12/Anushka-Virat1_18333434bb6_original-ratio.jpg",
      "https://i.pinimg.com/564x/13/67/d5/1367d5c6652026128500ae7c1b9ffd5e.jpg",
    ],
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://i.pinimg.com/564x/21/07/42/210742f4268f5e27e60c703eedc9ec06.jpg",
    ],
    [
      "virat.kohli",
      "https://imagevars.gulfnews.com/2022/09/12/Anushka-Virat1_18333434bb6_original-ratio.jpg",
      "https://i.pinimg.com/564x/96/b9/a6/96b9a67ae9bfb28905811c55416d164e.jpg",
    ],
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://i.pinimg.com/564x/dd/29/2a/dd292a2c1d72e799f3b5d217a253553c.jpg",
    ],
    [
      "virat.kohli",
      "https://imagevars.gulfnews.com/2022/09/12/Anushka-Virat1_18333434bb6_original-ratio.jpg",
      "https://i.pinimg.com/564x/37/d2/d3/37d2d3ed0a76fbd384ef57f6f5c1f020.jpg",
    ],
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://i.pinimg.com/564x/37/db/2c/37db2ce70f33161a13bbb7d8ba6dae96.jpg",
    ]
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
        child: ListView.builder(
      itemCount: reelData.length,
      itemBuilder: (context, index) {
        return Reel(
          userName: reelData[index][0],
          userPhoto: reelData[index][1],
          reelMedia: reelData[index][2],
        );
      },
    ));
  }
}

class Reel extends StatefulWidget {
  final String reelMedia;
  final String userPhoto;
  final String userName;

  const Reel({
    super.key,
    required this.userName,
    required this.userPhoto,
    required this.reelMedia,
  });

  @override
  State<StatefulWidget> createState() => _ReelState();
}

class _ReelState extends State<Reel> {
  bool isLike = false;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.black.withOpacity(0.95),
          child: Image.network(
            widget.reelMedia,
          ),
        ),
        Positioned(
          bottom: 200,
          right: 10,
          child: IconButton(
              onPressed: () {
                setState(() {
                  isLike = !isLike;
                });
              },
              icon: isLike
                  ? const Icon(
                      size: 30,
                      Icons.favorite,
                      color: Colors.red,
                    )
                  : const Icon(
                      size: 25,
                      Icons.favorite_border_outlined,
                      color: Colors.white,
                    )),
        ),
        Positioned(
          bottom: 150,
          right: 10,
          child: IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.comment,
              color: Colors.white,
              size: 30,
            ),
          ),
        ),
        Positioned(
          bottom: 100,
          right: 10,
          child: IconButton(
            onPressed: () {},
            icon: const Icon(
              Icons.send,
              color: Colors.white,
              size: 30,
            ),
          ),
        ),
        Positioned(
            bottom: 100,
            left: 20,
            child: Container(
              child: Row(
                children: [
                  Container(
                    padding: const EdgeInsets.all(1.0),
                    margin: const EdgeInsets.only(right: 20),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(
                          width: 2.5,
                          color: Color.fromARGB(206, 255, 254, 254)),
                    ),
                    child: CircleAvatar(
                      radius: 20.0,
                      backgroundImage: NetworkImage(widget.userPhoto),
                    ),
                  ),
                  Text(
                    widget.userName,
                    style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                ],
              ),
            )),
      ],
    );
  }
}
