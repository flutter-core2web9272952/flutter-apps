import 'package:astrophysics_app/moreInfoPage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:astrophysics_app/requiredClasses.dart';
import 'package:astrophysics_app/databaseFile.dart';

import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

class OtherInfo extends StatefulWidget {
  const OtherInfo({super.key});

  @override
  State<StatefulWidget> createState() {
    return _OtherInfoState();
  }
}

class _OtherInfoState extends State {
  List<SingleModalOthers> OtherData = [];

  //global database instance
  dynamic database;

  Future<void> initializeDatabase() async {
    database = mycreateDatabaseFunction();

//Code to insert Other data

    SingleModalOthers obj1 = SingleModalOthers(
      objectName: "Black Holes",
      objectImg:
          "https://i.pinimg.com/736x/e8/65/6e/e8656ec5143ebd5dd6559d2a976041e7.jpg",
      info:
          "Mysterious regions in space where gravity is so strong that nothing, not even light, can escape. They come in various sizes, from stellar-mass black holes to supermassive black holes at the centers of galaxies.",
      moreInfo:
          "Mysterious regions in space where gravity is so strong that nothing, not even light, can escape. They come in various sizes, from stellar-mass black holes to supermassive black holes at the centers of galaxies.",
      bigImg:
          "https://i.pinimg.com/736x/e8/65/6e/e8656ec5143ebd5dd6559d2a976041e7.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Black_hole",
    );

    SingleModalOthers obj2 = SingleModalOthers(
      objectName: "Neutron Stars",
      objectImg:
          "https://i.pinimg.com/564x/6f/68/39/6f68391f318910d6c2e24db943a870e3.jpg",
      info:
          "Extremely dense remnants of massive stars that have undergone supernova explosions. They are only about 20 kilometers (12 miles) in diameter but contain more mass than the Sun.",
      moreInfo:
          "Extremely dense remnants of massive stars that have undergone supernova explosions. They are only about 20 kilometers (12 miles) in diameter but contain more mass than the Sun.",
      bigImg:
          "https://i.pinimg.com/564x/6f/68/39/6f68391f318910d6c2e24db943a870e3.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Neutron_star",
    );

    SingleModalOthers obj3 = SingleModalOthers(
      objectName: "Pulsars",
      objectImg:
          "https://i.pinimg.com/564x/25/f0/60/25f060ef8e26c1012d3e137b97a5226d.jpg",
      info:
          "Highly magnetized, rotating neutron stars that emit beams of electromagnetic radiation. They are often referred to as cosmic lighthouses due to their precise and regular pulses.",
      moreInfo:
          "Highly magnetized, rotating neutron stars that emit beams of electromagnetic radiation. They are often referred to as cosmic lighthouses due to their precise and regular pulses.",
      bigImg:
          "https://i.pinimg.com/564x/25/f0/60/25f060ef8e26c1012d3e137b97a5226d.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Pulsar",
    );
    SingleModalOthers obj4 = SingleModalOthers(
      objectName: "Quasars",
      objectImg:
          "https://i.pinimg.com/564x/77/98/46/779846f9ae3076db7f5b1571da7dca08.jpg",
      info:
          "Extremely luminous and distant objects powered by supermassive black holes at the centers of galaxies. They emit more energy than entire galaxies, making them visible across vast cosmic distances.",
      moreInfo:
          "Extremely luminous and distant objects powered by supermassive black holes at the centers of galaxies. They emit more energy than entire galaxies, making them visible across vast cosmic distances.",
      bigImg:
          "https://i.pinimg.com/564x/77/98/46/779846f9ae3076db7f5b1571da7dca08.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Quasar",
    );

    SingleModalOthers obj5 = SingleModalOthers(
      objectName: "Nebulae",
      objectImg:
          "https://i.pinimg.com/564x/3a/b5/f3/3ab5f3509a72362185851cf6e66b837d.jpg",
      info:
          "Giant clouds of dust and gas in space where stars are born. They come in various shapes and colors, with some of the most famous ones including the Orion Nebula and the Eagle Nebula.",
      moreInfo:
          "Giant clouds of dust and gas in space where stars are born. They come in various shapes and colors, with some of the most famous ones including the Orion Nebula and the Eagle Nebula.",
      bigImg:
          "https://i.pinimg.com/564x/3a/b5/f3/3ab5f3509a72362185851cf6e66b837d.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Nebula",
    );

    SingleModalOthers obj6 = SingleModalOthers(
      objectName: "Gamma-Ray Bursts",
      objectImg:
          "https://i.pinimg.com/564x/d4/64/93/d464934f85e49fc5f9493fbe4d0c99f1.jpg",
      info:
          "The most energetic explosions in the universe, often associated with the deaths of massive stars or the collisions of neutron stars.",
      moreInfo:
          "The most energetic explosions in the universe, often associated with the deaths of massive stars or the collisions of neutron stars.",
      bigImg:
          "https://i.pinimg.com/564x/d4/64/93/d464934f85e49fc5f9493fbe4d0c99f1.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Gamma-ray_burst",
    );

    // insertOtherData(obj1);
    // insertOtherData(obj2);
    // insertOtherData(obj3);
    // insertOtherData(obj4);
    // insertOtherData(obj5);
    // insertOtherData(obj6);

    await fetchInitialValues();
  }

  Future<void> fetchInitialValues() async {
    OtherData = await fetchOtherData();
    print(OtherData);
    setState(() {});
  }

  Future<void> insertOtherData(SingleModalOthers obj) async {
    final localDB = await database;

    localDB.insert(
      "Others",
      obj.getOtherMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    print("Data inserted...");
  }

  Future<List<SingleModalOthers>> fetchOtherData() async {
    final localDb = await database;

    List<Map<String, dynamic>> mapEntry = await localDb.query(
      "Others",
    );

    return List.generate(mapEntry.length, (i) {
      return SingleModalOthers(
        id: mapEntry[i]["id"],
        objectName: mapEntry[i]["objectName"],
        objectImg: mapEntry[i]["objectImg"],
        info: mapEntry[i]["info"],
        moreInfo: mapEntry[i]["moreInfo"],
        bigImg: mapEntry[i]["bigImg"],
        websiteLink: mapEntry[i]["websiteLink"],
      );
    });
  }

  @override
  void initState() {
    super.initState();
    initializeDatabase();
  }

  bool isinput = false;
  int currindex = 0;

  TextEditingController nameTextController = TextEditingController();
  TextEditingController infoTextController = TextEditingController();
  TextEditingController webLinkTextController = TextEditingController();

  void addoject(bool doEdit, [SingleModalOthers? singleModelObject]) async {
    if (nameTextController.text.trim().isNotEmpty &&
        infoTextController.text.trim().isNotEmpty) {
      if (!doEdit) {
        //tmp variable to store website link
        String webLink =
            "https://i.pinimg.com/564x/a8/29/7a/a8297a1ae5ebc79ae020e38ac4e61a66.jpg";

        if (webLinkTextController.text.trim().isNotEmpty) {
          webLink = webLinkTextController.text.trim();
        }

        SingleModalOthers obj = SingleModalOthers(
          objectName: nameTextController.text.trim(),
          info: infoTextController.text.trim(),
          moreInfo: infoTextController.text.trim(),
          bigImg: webLink,
          objectImg: webLink,
          websiteLink: "https://en.wikipedia.org/wiki/Astronomical_object",
        );

        await insertOtherData(obj);
        OtherData = await fetchOtherData();

        setState(() {});
      }
    }

    clearControllers();
  }

  void clearControllers() {
    nameTextController.clear();
    infoTextController.clear();
  }

  void buildBottomsheet(bool doEdit, [SingleModalOthers? singleModelObject]) {
    showModalBottomSheet(
        backgroundColor: Colors.black,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Insert Object",
                    style: GoogleFonts.zenDots(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      controller: nameTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                          labelText: "Enter Object Name",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          labelStyle: TextStyle(fontStyle: FontStyle.italic),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1)),
                          )),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      controller: webLinkTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                          labelText: "Paste object img link from web.",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          labelStyle: TextStyle(fontStyle: FontStyle.italic),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1)),
                          )),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      maxLines: 4,
                      controller: infoTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                        labelText: "Enter Object Description",
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        labelStyle: TextStyle(fontStyle: FontStyle.italic),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          borderSide: BorderSide(
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    height: 50,
                    width: 300,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(30)),
                    child: ElevatedButton(
                        onPressed: () {
                          if (!isinput) {
                            addoject(doEdit, singleModelObject);
                          }
                          Navigator.of(context).pop();
                        },
                        style: const ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(
                                Color.fromARGB(255, 215, 85, 49))),
                        child: Text(
                          "Add Object",
                          style: GoogleFonts.zenDots(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                          ),
                        )),
                  ),
                  const SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.grey,
                ),
              ),
            ),
            ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: OtherData.length,
                separatorBuilder: (context, index) {
                  return const SizedBox(
                    height: 20,
                  );
                },
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      SingleModalObject object = SingleModalObject(
                        name: OtherData[index].objectName,
                        img: OtherData[index].objectImg,
                        info: OtherData[index].info,
                        moreInfo: OtherData[index].moreInfo,
                        bigImg: OtherData[index].bigImg,
                        websiteLink: OtherData[index].websiteLink,
                      );

                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return MoreInfoPage(object: object);
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 16, horizontal: 22),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.white,
                            width: 3,
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(20),
                          ),
                        ),
                        child: Row(
                          children: [
                            Image.network(
                              OtherData[index].objectImg,
                              width: 106,
                            ),
                            const SizedBox(
                              width: 6,
                            ),
                            Flexible(
                              child: Column(
                                children: [
                                  Text(
                                    OtherData[index].objectName,
                                    style: GoogleFonts.luckiestGuy(
                                      fontSize: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 13,
                                  ),
                                  Text(
                                    OtherData[index].info,
                                    style: GoogleFonts.zenDots(
                                        fontSize: 15,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            // clearControllers();
            buildBottomsheet(false);
          });
        },
        shape: const CircleBorder(),
        backgroundColor: Colors.grey,
        child: const Icon(
          color: Colors.white,
          Icons.add,
          size: 40.0,
        ),
      ),
    );
  }
}
