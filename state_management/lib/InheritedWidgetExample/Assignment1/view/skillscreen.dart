import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:state_management/InheritedWidgetExample/Assignment1/sharedData.dart';
import 'package:state_management/InheritedWidgetExample/Assignment1/view/infoscreen.dart';

class SkillPage extends StatefulWidget {
  const SkillPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SkillPageState();
  }
}

class _SkillPageState extends State {
  TextEditingController _skillController = TextEditingController();

  List<String> skills = [];

  @override
  Widget build(BuildContext context) {
    SharedData sharedObject = SharedData.of(context);

    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => InfoPage()));
              },
              icon: Icon(Icons.arrow_back_ios))
        ],
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                height: 40,
                width: 250,
                child: TextFormField(
                  controller: _skillController,
                  decoration: const InputDecoration(
                    hintText: "Enter skill",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          GestureDetector(
            onTap: () {
              skills.add(_skillController.text);

              print(sharedObject.data.skill);

              sharedObject.data.skill.add(_skillController.text);
              setState(() {});
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 7),
              decoration: BoxDecoration(
                color: Colors.purple.shade100,
                borderRadius: BorderRadius.circular(20),
              ),
              child: const Text(
                "Add",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          ListView.builder(
              shrinkWrap: true,
              itemCount: skills.length,
              itemBuilder: (context, index) {
                return Container(
                  alignment: Alignment.center,
                  child: Text(
                    skills[index],
                    style: const TextStyle(
                        fontSize: 19, fontWeight: FontWeight.w600),
                  ),
                );
              })
        ],
      ),
    );
  }
}
