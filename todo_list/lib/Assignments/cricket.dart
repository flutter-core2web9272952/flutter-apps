import 'dart:math';

import 'package:flutter/material.dart';

class TodoDemo extends StatefulWidget {
  const TodoDemo({super.key});

  @override
  State<StatefulWidget> createState() {
    return _TodoDemo();
  }
}

class _TodoDemo extends State {
  List<String> imgList = ["Hello", "Hi", "Bye"];
  List<String> imgList1 = ["Hello1", "Hi1", "Bye1"];

  int curIndex = 0;

  List<List<String>> data = [
    [
      "https://i.pinimg.com/564x/1e/f2/6e/1ef26ee26a673ebea6036414bf2b1b0d.jpg",
      "https://i.pinimg.com/564x/aa/75/e5/aa75e5dba9996d8bca0eada6f1f77ca5.jpg",
      "https://i.pinimg.com/564x/49/14/e5/4914e594b9401731369893e0d9028ae7.jpg",
    ],
    [
      "https://i.pinimg.com/564x/3a/f1/ad/3af1ad9785f3c519e3302c4b6185b866.jpg",
      "https://i.pinimg.com/564x/6b/59/e6/6b59e6bb50cfe88ba81357749b637474.jpg",
      "https://i.pinimg.com/564x/24/22/db/2422dbf3383b1c432d7e3e36fffa8a6b.jpg",
    ],
    [
      "https://i.pinimg.com/564x/e1/76/74/e176745db9c8184327163b97d53985a1.jpg",
      "https://i.pinimg.com/564x/ae/14/33/ae14337a7b297fbcac37e2fa742cfa95.jpg",
      "https://i.pinimg.com/564x/55/f2/f0/55f2f0dbbf7f5174619b6065640e929e.jpg",
    ]
  ];

  List<List<List<String>>> data1 = [
    [
      [
        "https://i.pinimg.com/564x/1e/f2/6e/1ef26ee26a673ebea6036414bf2b1b0d.jpg",
        "Virat Kohli",
      ],
      [
        "https://i.pinimg.com/564x/aa/75/e5/aa75e5dba9996d8bca0eada6f1f77ca5.jpg",
        "MS Dhoni",
      ],
      [
        "https://i.pinimg.com/564x/49/14/e5/4914e594b9401731369893e0d9028ae7.jpg",
        "Rohit Shama",
      ],
    ],
    [
      [
        "https://i.pinimg.com/564x/3a/f1/ad/3af1ad9785f3c519e3302c4b6185b866.jpg",
        "R. Jadeja",
      ],
      [
        "https://i.pinimg.com/564x/6b/59/e6/6b59e6bb50cfe88ba81357749b637474.jpg",
        "R Ashwin",
      ],
      [
        "https://i.pinimg.com/564x/24/22/db/2422dbf3383b1c432d7e3e36fffa8a6b.jpg",
        "Cheteshwar Pujara",
      ],
    ],
    [
      [
        "https://i.pinimg.com/564x/e1/76/74/e176745db9c8184327163b97d53985a1.jpg",
        "S. Yadav"
      ],
      [
        "https://i.pinimg.com/564x/ae/14/33/ae14337a7b297fbcac37e2fa742cfa95.jpg",
        "Shubhman Gill"
      ],
      [
        "https://i.pinimg.com/564x/55/f2/f0/55f2f0dbbf7f5174619b6065640e929e.jpg",
        "Hardik Pandy"
      ],
    ]
  ];

  List<String> format = ["One Day", "Test", "TTwenty"];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Cricket"),
        centerTitle: true,
        backgroundColor: Colors.blue,
      ),
      body: ListView.separated(
        itemCount: 3,
        separatorBuilder: (context, indxx) {
          return Text("");
        },
        itemBuilder: (context, index) {
          return Container(
            child: Column(
              children: [
                Text(
                  format[index],
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.w500),
                ),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder: (context, indx) {
                    return Container(
                      padding: const EdgeInsets.all(10),
                      color: Colors.black,
                      child: Container(
                        child: Column(
                          children: [
                            Image.network(data[index][indx]),
                            // Text(
                            //   data[index][index][0][1],
                            //   textAlign: TextAlign.center,
                            //   style: const TextStyle(
                            //       color: Colors.white,
                            //       fontSize: 20,
                            //       fontWeight: FontWeight.w500),
                            // )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
