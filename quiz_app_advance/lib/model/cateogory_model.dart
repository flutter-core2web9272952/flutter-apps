import 'dart:ffi';
import 'package:flutter/material.dart';

class CategoryModal {
  String icon;
  Color iconColor;
  String categoryName;

  CategoryModal({
    required this.icon,
    required this.categoryName,
    required this.iconColor,
  });
}
