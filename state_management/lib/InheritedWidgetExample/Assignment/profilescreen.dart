import 'package:flutter/material.dart';
import 'package:state_management/InheritedWidgetExample/Assignment/mydrawer.dart';
import 'package:state_management/InheritedWidgetExample/Assignment/sharedData.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State {
  @override
  Widget build(BuildContext context) {
    SharedData dataObj = SharedData.of(context);

    return Scaffold(
      appBar: AppBar(),
      drawer: const MyDrawer(),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 100,
                height: 100,
                padding: const EdgeInsets.all(3),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Colors.blue, width: 2),
                ),
                child: ClipOval(
                  child: Image.network(
                    dataObj.data["img"]!,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Text(
                dataObj.data["name"]!,
                style: const TextStyle(
                  fontSize: 20,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
