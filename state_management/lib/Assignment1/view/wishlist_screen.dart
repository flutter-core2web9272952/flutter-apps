import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:state_management/Assignment1/controllers/product_controller.dart';
import 'package:state_management/Assignment1/controllers/wishlist_controller.dart';

class WishList extends StatefulWidget {
  const WishList({super.key});

  @override
  State<StatefulWidget> createState() {
    return _WishListState();
  }
}

class _WishListState extends State {
  @override
  Widget build(BuildContext context) {
    var productControllerObject =
        Provider.of<ProviderProductController>(context, listen: false);
    var wishlistControllerObject =
        Provider.of<ProviderWishlistController>(context, listen: false);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Wish list"),
      ),
      body: Consumer<ProviderWishlistController>(
        builder: (create, value, child) {
          return ListView.builder(
            itemCount: wishlistControllerObject.data.length,
            itemBuilder: (context, index) {
              return Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                alignment: Alignment.bottomCenter,
                decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(
                    Radius.circular(20),
                  ),
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(0, 0),
                        spreadRadius: 0,
                        blurRadius: 10,
                        color: Color.fromARGB(94, 0, 0, 0))
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Image.network(
                          wishlistControllerObject.data[index].productImg,
                          width: 65,
                          height: 80,
                          fit: BoxFit.cover,
                        ),
                        const SizedBox(width: 20),
                        Text(
                          wishlistControllerObject.data[index].productName,
                          style: const TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                        const SizedBox(width: 20),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 10, vertical: 3),
                          decoration: BoxDecoration(
                            color: const Color.fromARGB(101, 255, 235, 59),
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Text(
                            "\$ ${wishlistControllerObject.data[index].productPrice}",
                            style: const TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Consumer(
                          builder: (context, value, child) {
                            return IconButton(
                              onPressed: () {
                                wishlistControllerObject
                                    .toggleFevoriteSingleProduct(index);

                                if (!wishlistControllerObject
                                    .data[index].isfavorite) {
                                  wishlistControllerObject.data.remove(
                                      wishlistControllerObject.data[index]);
                                }
                              },
                              icon: wishlistControllerObject
                                      .data[index].isfavorite
                                  ? const Icon(Icons.favorite)
                                  : const Icon(Icons.favorite_outline),
                            );
                          },
                        ),
                        Spacer(),
                        Container(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text("Quantity:"),
                              const SizedBox(width: 5),
                              Consumer(
                                builder: (create, value, child) {
                                  return Text(
                                    "${wishlistControllerObject.data[index].quantity}",
                                  );
                                },
                              ),
                              const SizedBox(width: 5),
                            ],
                          ),
                        )
                      ],
                    )
                  ],
                ),
              );
            },
          );
        },
      ),
    );
  }
}
