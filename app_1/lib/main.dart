import 'package:app_1/assignmnt2.dart';
import 'package:app_1/assignment3.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: Assignment2());
  }
}
