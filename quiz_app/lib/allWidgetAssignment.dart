import 'package:flutter/material.dart';

class Assignment extends StatefulWidget {
  const Assignment({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Assignment();
  }
}

class _Assignment extends State {
  List<List<String>> info = [
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Sandesh Marathe",
      "PICT",
      "BE IT",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Avishkar Jagtap ",
      "SKN",
      "BE COMP",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Sumit Kawale",
      "PICT",
      "BE IT",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Suraj Gaikwad",
      "VIT",
      "BTECH COMP",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Ayush Mishra",
      "VIIT",
      "BTECH COMP",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Suyog Pisal",
      "SITS",
      "BE COMP",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Ratnesh Ankam",
      "SKN",
      "BE COMPM",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Prathamesh Tikekar",
      "SKN",
      "BE COMP",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Omkar Pawale",
      "DY Patil",
      "TE AIDS",
    ],
    [
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSQq6gaTf6N93kzolH98ominWZELW881HqCgw&usqp=CAU",
      "Manik Tambulkar",
      "NBN",
      "BE COMP",
    ],
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: const Text("Assignment..."),
        actions: const [
          Icon(Icons.person_outlined),
          SizedBox(
            width: 10,
          ),
          Icon(Icons.settings),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.purple,
        onPressed: () {},
        child: const Icon(Icons.home),
      ),
      body: Container(
          child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 15),
              child: Row(children: [
                const SizedBox(
                  width: 80,
                ),
                Container(
                  alignment: Alignment.center,
                  width: 500,
                  padding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 100),
                  decoration: BoxDecoration(
                      border: Border.all(width: 0.5),
                      borderRadius: BorderRadius.circular(30)),
                  child: const Text(
                    "Search",
                    style: TextStyle(fontSize: 15),
                  ),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.search),
                )
              ]),
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                child: Row(children: [
                  const Text("Filter by:"),
                  const SizedBox(
                    width: 30,
                  ),
                  Container(
                    width: 100,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: Text("College"),
                    ),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  Container(
                    width: 100,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: Text("Branch"),
                    ),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  Container(
                    width: 100,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: Text("Year"),
                    ),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  Container(
                    width: 100,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: Text("Location"),
                    ),
                  ),
                  const SizedBox(
                    width: 30,
                  ),
                  Container(
                    width: 100,
                    child: ElevatedButton(
                      onPressed: () {},
                      child: Text("IIT'n"),
                    ),
                  )
                ]),
              ),
            ),
            ListView.builder(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemCount: info.length,
                itemBuilder: (context, index) {
                  return Container(
                    margin: const EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey),
                        borderRadius: BorderRadius.circular(10)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Image.network(
                          info[index][0],
                          width: 100,
                        ),
                        Container(
                          width: 400,
                          alignment: Alignment.center,
                          child: Column(
                            children: [
                              Text(
                                info[index][1],
                                style: const TextStyle(
                                    fontSize: 20, fontWeight: FontWeight.w500),
                              ),
                              const SizedBox(
                                height: 30,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text("College: ${info[index][2]}"),
                                  const SizedBox(
                                    width: 100,
                                  ),
                                  Text("Branch: ${info[index][3]}")
                                ],
                              )
                            ],
                          ),
                        ),
                        ElevatedButton(
                            onPressed: () {}, child: const Text("View Data"))
                      ],
                    ),
                  );
                }),
          ],
        ),
      )),
    );
  }
}
