import 'package:flutter/material.dart';

class LifeCycle extends StatefulWidget {
  const LifeCycle({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LifeCycleState();
  }
}

bool showScreenFLg = true;

Widget screen() {
  if (showScreenFLg) {
    return Object1();
  } else {
    return Object2();
  }
}

class _LifeCycleState extends State {
  @override
  Widget build(BuildContext context) {
    return screen();
  }
}

class Object1 extends StatefulWidget {
  const Object1({super.key});

  @override
  State<StatefulWidget> createState() {
    print("Inside createState");
    return _Object1State();
  }
}

class _Object1State extends State {
  @override
  void initState() {
    print("Inside initState");
    super.initState();
  }

  @override
  void didUpdateWidget(Object1 wid) {
    print("Inside didUpdateWidget");
    super.didUpdateWidget(wid);
  }

  @override
  void didUpdateDependencies() {
    print("inside didUpdateDependencies");
    super.didChangeDependencies();
  }

  @override
  void deactivate() {
    print("inside deactivate");
    super.deactivate();
  }

  @override
  void despose() {
    print("inside despose");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("Inside build");

    return Scaffold(
      appBar: AppBar(
        title: const Text("Widget 1 "),
        backgroundColor: Colors.red,
      ),
      body: Center(
          child: Column(
        children: [
          ElevatedButton(
            onPressed: () {
              print(showScreenFLg);
              showScreenFLg = !showScreenFLg;
              setState(() {});
            },
            child: const Text(
              "Get Widget 2",
            ),
          ),
        ],
      )),
    );
  }
}

class Object2 extends StatefulWidget {
  const Object2({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Object2State();
  }
}

class _Object2State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Widget 2 "),
        backgroundColor: Colors.blue,
      ),
      body: Center(
          child: Column(
        children: [
          ElevatedButton(
            onPressed: () {
              showScreenFLg = !showScreenFLg;

              setState(() {});
            },
            child: const Text(
              "Get Widget 1",
            ),
          ),
        ],
      )),
    );
  }
}
