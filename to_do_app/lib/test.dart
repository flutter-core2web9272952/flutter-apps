import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';

class Test extends StatefulWidget {
  const Test({super.key});

  @override
  State<StatefulWidget> createState() {
    return _Test();
  }
}

class _Test extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Animate(
      delay: 500.ms,
      effects: [FadeEffect(), ScaleEffect()],
      child: Center(
        child: Text(
          "Hello",
          style: TextStyle(fontSize: 20),
        ),
      ),
    ));
  }
}
