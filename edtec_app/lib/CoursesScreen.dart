import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CoursesScreen extends StatefulWidget {
  const CoursesScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _CoursesScreen();
  }
}

class _CoursesScreen extends State {
  List<Map> coursesList = [
    {
      "title": "Introduction",
      "description": "Lorem Ipsum is simply dummy text ... "
    },
    {
      "title": "Introduction",
      "description": "Lorem Ipsum is simply dummy text ... "
    },
    {
      "title": "Introduction",
      "description": "Lorem Ipsum is simply dummy text ... "
    },
    {
      "title": "Introduction",
      "description": "Lorem Ipsum is simply dummy text ... "
    },
    {
      "title": "Introduction",
      "description": "Lorem Ipsum is simply dummy text ... "
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.center,
          stops: [.01, .6],
          colors: [
            Color.fromRGBO(197, 4, 98, 1),
            Color.fromRGBO(80, 3, 112, 1)
          ],
        )),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 47,
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: const Icon(
                          Icons.arrow_back,
                          size: 26,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  Text(
                    "UX Designer from Scratch.",
                    style: GoogleFonts.jost(
                        fontSize: 32.61,
                        fontWeight: FontWeight.w500,
                        color: Colors.white),
                  ),
                  Text(
                    "Basic guideline & tips & tricks for how to become a UX designer easily.",
                    style: GoogleFonts.jost(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: const Color.fromRGBO(228, 205, 225, 1)),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        child: Row(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(10),
                              decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: Color.fromRGBO(0, 82, 178, 1),
                              ),
                              child: const Icon(
                                Icons.person_2_outlined,
                                color: Colors.white,
                                size: 15,
                              ),
                            ),
                            const SizedBox(
                              width: 7,
                            ),
                            Text(
                              "Author: ",
                              style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: const Color.fromRGBO(190, 154, 197, 1),
                              ),
                            ),
                            Text(
                              "Jenny: ",
                              style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w400,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text(
                              "4.8",
                              style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: const Color.fromRGBO(190, 154, 197, 1),
                              ),
                            ),
                            const Icon(
                              Icons.star,
                              size: 15,
                              color: Color.fromRGBO(255, 146, 0, 1),
                            ),
                            Text(
                              "(20 review)",
                              style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: const Color.fromRGBO(190, 154, 197, 1),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(38),
                  topRight: Radius.circular(38),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(30),
                child: SingleChildScrollView(
                  child: ListView.separated(
                      shrinkWrap: true,
                      itemCount: coursesList.length,
                      separatorBuilder: (context, index) {
                        return const SizedBox(
                          height: 20,
                        );
                      },
                      itemBuilder: (contex, index) {
                        return Container(
                          padding: const EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                offset: Offset(0, 8),
                                blurRadius: 40,
                                color: Color.fromRGBO(0, 0, 0, 0.15),
                              )
                            ],
                          ),
                          child: Row(
                            children: [
                              Container(
                                padding: const EdgeInsets.all(11),
                                decoration: const BoxDecoration(
                                    color: Color.fromRGBO(230, 239, 239, 1),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(12))),
                                child: const Icon(Icons.play_circle_outlined),
                              ),
                              const SizedBox(
                                width: 11,
                              ),
                              Flexible(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Introduction",
                                      style: GoogleFonts.jost(
                                          fontSize: 17.61,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black),
                                    ),
                                    const SizedBox(
                                      height: 2,
                                    ),
                                    Text(
                                      "Lorem Ipsum is simply dummy text ... ",
                                      style: GoogleFonts.jost(
                                          fontSize: 12.61,
                                          fontWeight: FontWeight.w500,
                                          color: const Color.fromRGBO(
                                              143, 143, 143, 1)),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        );
                      }),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
