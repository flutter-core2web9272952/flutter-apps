import 'package:flutter/material.dart';

class CompanyModal extends ChangeNotifier {
  String compName;
  int empCount;

  CompanyModal({
    required this.compName,
    required this.empCount,
  });
}
