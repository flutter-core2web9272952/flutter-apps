class Data {
  int? id;
  String? employeeName;
  int? employeeSalary;
  int? employeeAge;
  String? profileImage;

  Data(Map<String, dynamic> json) {
    id = json["id"];
    employeeName = json["employee_name"];
    employeeSalary = json["employee_salary"];
    employeeAge = json["employee_age"];
    profileImage = json["profile_image"];
  }
}


/*


class EmployeeModalClass  {
  String? status;
  List<Data>? data;
  String? message;

  EmployeeModalClass() {
    status = "";
    message = "";
    data = Data();
  }

  EmployeeModalClass.fromjson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = Data.fromjson(json['data']);
  }
}

class Data {
  int? id;
  String? employeeName;
  int? employeeSalary;
  int? employeeAge;
  String? profileImage;

  Data() {
    id = 0;
    employeeName = "";
    employeeSalary = 0;
    employeeAge = 0;
    profileImage = "";
  }

  Data.fromjson(Map<String, dynamic> json) {
    id = json["id"];
    employeeName = json["employee_name"];
    employeeSalary = json["employee_salary"];
    employeeAge = json["employee_age"];
    profileImage = json["profile_image"];
  }
}



*/