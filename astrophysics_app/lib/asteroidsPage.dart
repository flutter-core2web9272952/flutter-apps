import 'package:astrophysics_app/moreInfoPage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:astrophysics_app/requiredClasses.dart';

import 'package:astrophysics_app/databaseFile.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as p;
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

class AsteroidInfo extends StatefulWidget {
  const AsteroidInfo({super.key});

  @override
  State<StatefulWidget> createState() {
    return _AsteroidInfoState();
  }
}

class _AsteroidInfoState extends State {
  List<SingleModalAsteroid> asteroidsData = [];

  //global database instance
  dynamic database;

  Future<void> initializeDatabase() async {

    database = mycreateDatabaseFunction();

    // Code to insert Asteroids data

    SingleModalAsteroid obj1 = SingleModalAsteroid(
      asteroidName: "Ceres",
      asteroidImg:
          "https://i.pinimg.com/564x/66/ad/83/66ad83aaeebea2ad193a0177c21ae4ed.jpg",
      info:
          "The largest object in the asteroid belt between Mars and Jupiter, Ceres is classified as a dwarf planet. It has a diameter of about 940 kilometers and was the first asteroid to be discovered, in 1801.",
      moreInfo:
          "The largest object in the asteroid belt between Mars and Jupiter, Ceres is classified as a dwarf planet. It has a diameter of about 940 kilometers and was the first asteroid to be discovered, in 1801.",
      bigImg:
          "https://i.pinimg.com/564x/66/ad/83/66ad83aaeebea2ad193a0177c21ae4ed.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Ceres_(dwarf_planet)",
    );

    SingleModalAsteroid obj2 = SingleModalAsteroid(
      asteroidName: "Vesta",
      asteroidImg:
          "https://i.pinimg.com/564x/b6/da/1c/b6da1c9a3020ed556a82a8c47eed3653.jpg",
      info:
          "The second-largest object in the asteroid belt, Vesta is notable for its relatively large size and differentiated composition. NASA's Dawn spacecraft orbited and studied Vesta from 2011 to 2012.",
      moreInfo:
          "The second-largest object in the asteroid belt, Vesta is notable for its relatively large size and differentiated composition. NASA's Dawn spacecraft orbited and studied Vesta from 2011 to 2012.",
      bigImg:
          "https://i.pinimg.com/564x/b6/da/1c/b6da1c9a3020ed556a82a8c47eed3653.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/4_Vesta",
    );

    SingleModalAsteroid obj3 = SingleModalAsteroid(
      asteroidName: "Eros",
      asteroidImg:
          "https://i.pinimg.com/564x/5c/b7/f5/5cb7f5735c55cafb9e8a7153e20e9cbc.jpg",
      info:
          "A near-Earth asteroid and the first asteroid to be orbited by a spacecraft (NASA's NEAR Shoemaker) in 2000. It provided valuable insights into the composition and structure of asteroids.",
      moreInfo:
          "A near-Earth asteroid and the first asteroid to be orbited by a spacecraft (NASA's NEAR Shoemaker) in 2000. It provided valuable insights into the composition and structure of asteroids.",
      bigImg:
          "https://i.pinimg.com/564x/5c/b7/f5/5cb7f5735c55cafb9e8a7153e20e9cbc.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/433_Eros",
    );
    SingleModalAsteroid obj4 = SingleModalAsteroid(
      asteroidName: "Ida",
      asteroidImg:
          "https://i.pinimg.com/564x/da/68/ac/da68ac2f3b670ff5c7a58db0ecd41fcc.jpg",
      info:
          "Ida was the second asteroid to be visited by a spacecraft (Galileo) in 1993. It was discovered to have a small moon named Dactyl, the first confirmed asteroid moon.",
      moreInfo:
          "Ida was the second asteroid to be visited by a spacecraft (Galileo) in 1993. It was discovered to have a small moon named Dactyl, the first confirmed asteroid moon.",
      bigImg:
          "https://i.pinimg.com/564x/da/68/ac/da68ac2f3b670ff5c7a58db0ecd41fcc.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/243_Ida",
    );

    SingleModalAsteroid obj5 = SingleModalAsteroid(
      asteroidName: "Mathilde",
      asteroidImg:
          "https://i.pinimg.com/564x/e2/9b/b4/e29bb4aee193c05c0d7a2e1dfc6c7d94.jpg",
      info:
          "Visited by the NEAR Shoemaker spacecraft in 1997, Mathilde is one of the largest asteroids in the asteroid belt. Its heavily cratered surface provided insights into the early history of the solar system.",
      moreInfo:
          "Visited by the NEAR Shoemaker spacecraft in 1997, Mathilde is one of the largest asteroids in the asteroid belt. Its heavily cratered surface provided insights into the early history of the solar system.",
      bigImg:
          "https://i.pinimg.com/564x/e2/9b/b4/e29bb4aee193c05c0d7a2e1dfc6c7d94.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/253_Mathilde",
    );

    SingleModalAsteroid obj6 = SingleModalAsteroid(
      asteroidName: "Oumuamua",
      asteroidImg:
          "https://i.pinimg.com/564x/3b/0e/10/3b0e10970c5cb89acca950b265147612.jpg",
      info:
          "Oumuamua is an interstellar object that passed through our solar system in 2017. It gained significant attention from scientists and the public due to its unusual characteristics and the fact that it was the first confirmed object from outside the solar system to be observed passing through.",
      moreInfo:
          "Oumuamua is an interstellar object that passed through our solar system in 2017. It gained significant attention from scientists and the public due to its unusual characteristics and the fact that it was the first confirmed object from outside the solar system to be observed passing through.",
      bigImg:
          "https://i.pinimg.com/564x/3b/0e/10/3b0e10970c5cb89acca950b265147612.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/%CA%BBOumuamua",
    );

    // insertAsteroidsData(obj1);
    // insertAsteroidsData(obj2);
    // insertAsteroidsData(obj3);
    // insertAsteroidsData(obj4);
    // insertAsteroidsData(obj5);
    // insertAsteroidsData(obj6);

    await fetchInitialValues();
  }

  Future<void> fetchInitialValues() async {
    asteroidsData = await fetchAsteroidsData();
    print(asteroidsData);
    setState(() {});
  }

  Future<void> insertAsteroidsData(SingleModalAsteroid obj) async {
    final localDB = await database;

    localDB.insert(
      "Asteroids",
      obj.getAsteroidMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<SingleModalAsteroid>> fetchAsteroidsData() async {
    final localDb = await database;

    List<Map<String, dynamic>> mapEntry = await localDb.query(
      "Asteroids",
    );

    return List.generate(mapEntry.length, (i) {
      return SingleModalAsteroid(
        id: mapEntry[i]["id"],
        asteroidName: mapEntry[i]["asteroidName"],
        asteroidImg: mapEntry[i]["asteroidImg"],
        info: mapEntry[i]["info"],
        moreInfo: mapEntry[i]["moreInfo"],
        bigImg: mapEntry[i]["bigImg"],
        websiteLink: mapEntry[i]["websiteLink"],
      );
    });
  }

  @override
  void initState() {
    super.initState();
    initializeDatabase();
  }

  bool isinput = false;
  int currindex = 0;

  TextEditingController nameTextController = TextEditingController();
  TextEditingController infoTextController = TextEditingController();
  TextEditingController webLinkTextController = TextEditingController();

  void addoject(bool doEdit, [SingleModalGalaxy? singleModelObject]) async {
    if (nameTextController.text.trim().isNotEmpty &&
        infoTextController.text.trim().isNotEmpty) {
      if (!doEdit) {
        //tmp variable to store website link
        String webLink =
            "https://i.pinimg.com/564x/9d/45/8b/9d458b497db741d92be13376c4017682.jpg";

        if (webLinkTextController.text.trim().isNotEmpty) {
          webLink = webLinkTextController.text.trim();
        }

        SingleModalAsteroid obj = SingleModalAsteroid(
          asteroidName: nameTextController.text.trim(),
          info: infoTextController.text.trim(),
          moreInfo: infoTextController.text.trim(),
          bigImg: webLink,
          asteroidImg: webLink,
          websiteLink: "https://en.wikipedia.org/wiki/Asteroid",
        );

        await insertAsteroidsData(obj);
        asteroidsData = await fetchAsteroidsData();

        setState(() {});
      }
    }

    clearControllers();
  }

  void clearControllers() {
    nameTextController.clear();
    infoTextController.clear();
  }

  void buildBottomsheet(bool doEdit, [SingleModalGalaxy? singleModelObject]) {
    showModalBottomSheet(
        backgroundColor: Colors.black,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Insert Asteroid",
                    style: GoogleFonts.zenDots(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      controller: nameTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                          labelText: "Enter Object Name",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          labelStyle: TextStyle(fontStyle: FontStyle.italic),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1)),
                          )),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      controller: webLinkTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                          labelText: "Paste asteroid img link from web.",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          labelStyle: TextStyle(fontStyle: FontStyle.italic),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1)),
                          )),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      maxLines: 4,
                      controller: infoTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                        labelText: "Enter Object Description",
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        labelStyle: TextStyle(fontStyle: FontStyle.italic),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          borderSide: BorderSide(
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    height: 50,
                    width: 300,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(30)),
                    child: ElevatedButton(
                        onPressed: () {
                          if (isinput) {
                            addoject(doEdit, singleModelObject);
                          } else {
                            addoject(doEdit);
                          }
                          Navigator.of(context).pop();
                        },
                        style: const ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(
                                Color.fromARGB(255, 215, 85, 49))),
                        child: Text(
                          "Add Object",
                          style: GoogleFonts.zenDots(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                          ),
                        )),
                  ),
                  const SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.grey,
                ),
              ),
            ),
            ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: asteroidsData.length,
                separatorBuilder: (context, index) {
                  return const SizedBox(
                    height: 20,
                  );
                },
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      SingleModalObject object = SingleModalObject(
                        name: asteroidsData[index].asteroidName,
                        img: asteroidsData[index].asteroidImg,
                        info: asteroidsData[index].info,
                        moreInfo: asteroidsData[index].moreInfo,
                        bigImg: asteroidsData[index].bigImg,
                        websiteLink: asteroidsData[index].websiteLink,
                      );

                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return MoreInfoPage(object: object);
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 16, horizontal: 22),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.white,
                            width: 3,
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(20),
                          ),
                        ),
                        child: Row(
                          children: [
                            Image.network(
                              asteroidsData[index].asteroidImg,
                              width: 106,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Column(
                                children: [
                                  Text(
                                    asteroidsData[index].asteroidName,
                                    style: GoogleFonts.luckiestGuy(
                                      fontSize: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    asteroidsData[index].info,
                                    style: GoogleFonts.zenDots(
                                        fontSize: 14,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            // clearControllers();
            buildBottomsheet(false);
          });
        },
        shape: const CircleBorder(),
        backgroundColor: Colors.grey,
        child: const Icon(
          color: Colors.white,
          Icons.add,
          size: 40.0,
        ),
      ),
    );
  }
}
