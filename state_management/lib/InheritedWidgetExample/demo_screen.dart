import 'package:flutter/material.dart';
import 'package:state_management/InheritedWidgetExample/example.dart';

class Screen extends StatefulWidget {
  const Screen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ScreenState();
  }
}

class _ScreenState extends State {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DisplayInfo(),
    );
  }
}

class DisplayInfo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DisplayInfoState();
  }
}

class _DisplayInfoState extends State {
  @override
  Widget build(BuildContext context) {
    SharedData obj = SharedData.of(context);
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "${obj.data}",
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w600,
              ),
            ),
            const GetData(),
          ],
        ),
      ),
    );
  }
}

class GetData extends StatelessWidget {
  const GetData({super.key});

  @override
  Widget build(BuildContext context) {
    SharedData obj = SharedData.of(context);

    obj.data = 100;

    return Text(
      "${obj.data}",
      style: const TextStyle(
        fontSize: 20,
        fontWeight: FontWeight.w600,
      ),
    );
  }
}
