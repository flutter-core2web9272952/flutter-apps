import 'package:astrophysics_app/databaseFile.dart';
import 'package:astrophysics_app/moreInfoPage.dart';
import 'package:astrophysics_app/requiredClasses.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

class PlanetsInfo extends StatefulWidget {
  const PlanetsInfo({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PlanetsInfoState();
  }
}

class _PlanetsInfoState extends State {
  List<SingleModalPlanet> planetsData = [];

  dynamic database;

  Future<void> initializeDatabase() async {
    // this syntax is to create database on Desktop applications
    database = mycreateDatabaseFunction();

    // Code to insert planets data
    SingleModalPlanet mercury = SingleModalPlanet(
      planetName: "Mercury",
      planetImg:
          "https://i.pinimg.com/564x/ef/b9/a7/efb9a739957fa42669e8e92dd50fb05a.jpg",
      info:
          "The smallest planet, closest to the Sun, with extreme temperature variations and a cratered surface.",
      moreInfo:
          '''   Mercury, named after the Roman messenger god, is the smallest and innermost planet in our solar system, situated closest to the Sun. With a diameter of just 4,880 kilometers (3,032 miles), Mercury is only slightly larger than Earth's Moon. Its surface is heavily cratered, resembling the Moon, due to impacts from asteroids and comets over billions of years. Mercury lacks a significant atmosphere, resulting in extreme temperature variations between its scorching daytime highs and frigid nighttime lows. During the day, temperatures can soar to about 430°C (800°F), while at night, they plummet to around -180°C (-290°F). This stark temperature difference is due to Mercury's lack of atmosphere to retain heat.
        
          Despite its small size, Mercury possesses a substantial iron core, accounting for about 60% of its mass, making it the most metal-rich planet in the solar system. This large core contributes to its relatively high density, second only to Earth's. Mercury's surface features include vast plains, towering cliffs, and numerous craters, some of which contain volatile substances such as water ice in permanently shadowed regions near its poles. The planet's proximity to the Sun makes it challenging to observe from Earth, with limited opportunities for exploration.Nevertheless, several space missions, including NASA's MESSENGER (MErcury Surface, Space ENvironment, GEochemistry, and Ranging) mission, have provided valuable insights into Mercury's composition, geology, and magnetic field, enhancing our understanding of the dynamics and evolution of terrestrial planets.
      ''',
      bigImg:
          "https://i.pinimg.com/originals/ff/c7/ab/ffc7ab1f78d352c6d56719c7b3f5f0bb.gif",
      websiteLink: "https://en.wikipedia.org/wiki/Mercury_(planet)",
    );

    SingleModalPlanet venus = SingleModalPlanet(
      planetName: "Venus",
      planetImg:
          "https://i.pinimg.com/564x/f2/bb/92/f2bb92d82c6c4cb05038228610bc7475.jpg",
      info:
          "Known for its thick, toxic atmosphere and extreme greenhouse effect, making it the hottest planet in the solar system.",
      moreInfo:
          '''   Venus, often referred to as Earth's "sister planet," is the second planet from the Sun in our solar system and the closest planet to Earth. Named after the Roman goddess of love and beauty, Venus is known for its stunning brightness and is often visible to the naked eye as one of the brightest objects in the night sky. With a diameter of approximately 12,104 kilometers (7,521 miles), Venus is slightly smaller than Earth, yet it shares many similarities, such as its rocky composition and similar gravity. However, Venus is shrouded in a thick atmosphere primarily composed of carbon dioxide with traces of sulfuric acid, creating a dense and toxic environment with surface pressures about 92 times that of Earth's atmosphere.
        
        The surface of Venus is characterized by vast plains, highland regions, and thousands of volcanoes, including vast shield volcanoes and towering volcanic mountains. One of the most prominent features on Venus is its extensive network of deep canyons and channels, suggesting past geological activity. Despite its proximity to the Sun, Venus rotates very slowly on its axis, with a day on Venus lasting longer than its year, resulting in extreme temperature variations between its scorching hot days and frigid nights. The planet's dense atmosphere also contributes to a runaway greenhouse effect, making Venus the hottest planet in the solar system, with surface temperatures soaring to about 465°C (869°F), hotter than the surface of Mercury despite being farther from the Sun. Despite its hostile conditions, Venus remains an intriguing  object of study for scientists seeking to understand the mechanisms driving its extreme climate and geological processes.''',
      bigImg:
          "https://i.pinimg.com/originals/4a/c3/5f/4ac35fbe0a5e17ce00bd20d112f16192.gif",
      websiteLink: "https://en.wikipedia.org/wiki/Venus",
    );

    SingleModalPlanet earth = SingleModalPlanet(
      planetName: "Earth",
      planetImg:
          "https://i.pinimg.com/564x/b6/7c/e6/b67ce69117cb632318109f5cf3df8e11.jpg",
      info:
          "The only known planet with life, characterized by its diverse ecosystems, abundant water, and a protective atmosphere.",
      moreInfo:
          ''' Earth, our home planet, is the third planet from the Sun in the solar system and the only known celestial body to support life. With a diameter of approximately 12,742 kilometers (7,918 miles), Earth is the largest terrestrial planet and boasts a diverse range of environments, including vast oceans, towering mountains, lush forests, and expansive deserts. It is the perfect distance from the Sun to maintain temperatures conducive to life, with surface temperatures averaging around 14°C (57°F). Earth's atmosphere plays a crucial role in regulating its climate and protecting life by providing oxygen for respiration and shielding the planet from harmful solar radiation.
            
            Earth is home to an astonishing array of life forms, from microscopic bacteria to majestic mammals, all interconnected within a complex web of ecosystems. It harbors billions of humans, each contributing to the rich tapestry of cultures and civilizations that have flourished across the planet's surface throughout history. Beyond its biological diversity, Earth also boasts geological wonders, including tectonic activity that shapes its continents, volcanic eruptions that sculpt its landscapes, and erosion processes that continually reshape its surface features. The study of Earth, known as Earth science or geoscience, encompasses various disciplines such as geology, meteorology, oceanography, and ecology, all aimed at understanding the dynamic processes that govern our planet and sustain life as we know it.
''',
      bigImg:
          "https://i.pinimg.com/originals/8b/a2/3a/8ba23a54c5cf4a1a10bb846e307d2036.gif",
      websiteLink: "https://en.wikipedia.org/wiki/Mars",
    );
    SingleModalPlanet mars = SingleModalPlanet(
      planetName: "Mars",
      planetImg:
          "https://i.pinimg.com/564x/eb/69/87/eb69874a74d01756ed148faa6ed92125.jpg",
      info:
          "Often called the Red Planet, known for its rusty color due to iron-rich soil, and it has polar ice caps and evidence of past water flows.",
      moreInfo: '''
      Mars, often called the "Red Planet" due to its rusty reddish appearance, is the fourth planet from the Sun in our solar system. With a diameter of about 6,779 kilometers (4,212 miles), Mars is smaller than Earth, yet it shares several similarities with our home planet. Mars has distinct polar ice caps composed of water ice and frozen carbon dioxide, and its surface features include vast deserts, rocky mountains, and ancient river valleys, suggesting past liquid water flows. The Martian atmosphere is thin and composed  primarily of carbon dioxide, with traces of nitrogen and argon, and its surface experiences temperature extremes ranging from as low as -140°C (-220°F) at the poles to as high as 20°C (68°F) near the equator.
        
        Mars has long captured the imagination of scientists and the public alike as a potential abode for life beyond Earth.  Numerous robotic missions have explored its surface, seeking evidence of past or present life and studying its geology and climate. Recent discoveries, such as seasonal fluctuations of methane and the presence of liquid water beneath its surface, have fueled speculation about the planet's habitability. Moreover, plans for human missions to Mars are being actively pursued by space agencies and private companies, with the goal of establishing a sustained human presence on the Red Planet in the coming decades. Understanding Mars is not only crucial for unraveling the mysteries of our neighboring planet but also for advancing our understanding of planetary evolution and the potential for life beyond Earth.
''',
      bigImg:
          "https://i.pinimg.com/originals/38/bc/56/38bc56565ead27975dadedaa937f6840.gif",
      websiteLink: "https://en.wikipedia.org/wiki/Mars",
    );
    SingleModalPlanet jupyter = SingleModalPlanet(
      planetName: "Jupyter",
      planetImg:
          "https://i.pinimg.com/564x/92/e0/ce/92e0ce4388b8fc04592447803cbea45d.jpg",
      info:
          "The largest planet, known for its iconic Great Red Spot and numerous moons, with a turbulent atmosphere of gas giants.",
      moreInfo:
          '''   Jupiter, the largest planet in our solar system, is a gas giant located fifth from the Sun. It is named after the king of the Roman gods due to its immense size and gravitational influence. With a diameter of approximately 139,822 kilometers (86,881 miles), Jupiter is more than 11 times wider than Earth. Its atmosphere is predominantly composed of hydrogen and helium, with swirling bands of colorful clouds and massive storm systems, the most famous being the Great Red Spot, a gigantic storm larger than Earth, persisting for centuries. Jupiter also boasts a strong magnetic field and an extensive system of moons, with the four largest—Io, Europa, Ganymede, and Callisto, known as the Galilean moons—being particularly notable for their diverse geological features and potential for hosting subsurface oceans.
          
            Jupiter plays a crucial role in shaping the dynamics of the solar system, acting as a gravitational anchor that influences the orbits of neighboring planets and deflects potentially hazardous asteroids and comets away from Earth. Its immense size and mass make it a natural laboratory for studying the processes of planetary formation and evolution. Various space missions, including NASA's Juno spacecraft have provided valuable insights into Jupiter's atmosphere, magnetic field, and internal structure, enhancing our understanding of gas giants  and their role in the broader context of planetary systems. Jupiter continues to fascinate scientists and astronomers as a source of intrigue and discovery, offering tantalizing clues about the formation and diversity of worlds beyond our own.''',
      bigImg:
          "https://i.pinimg.com/originals/b7/aa/34/b7aa34af8c740f14a7e60c88893c63e6.gif",
      websiteLink: "https://en.wikipedia.org/wiki/Jupiter",
    );

    SingleModalPlanet saturn = SingleModalPlanet(
      planetName: "Saturn",
      planetImg:
          "https://i.pinimg.com/564x/1b/46/b1/1b46b1f5934d527119a4da66f78f4c25.jpg",
      info:
          "Famous for its spectacular ring system, composed of ice and rock particles, and it has numerous moons, including Titan.",
      moreInfo:
          '''   Saturn, renowned for its stunning ring system, is the sixth planet from the Sun in our solar system. Named after the Roman god of agriculture, Saturn is a gas giant characterized by its distinct yellowish hue and extensive ring system composed of icy particles ranging in size from micrometers to meters. With a diameter of approximately 116,460 kilometers (72,366 miles), Saturn is the second-largest planet, surpassed only by Jupiter. Its atmosphere, primarily composed of hydrogen and helium, exhibits colorful bands of clouds and swirling storm systems, including the iconic hexagonal-shaped storm at its north pole. Saturn also boasts a diverse array of moons, with over 80 known natural satellites, including Titan, the second-largest moon in the solar system, known for its thick atmosphere and methane lakes.
          
          Saturn's rings, consisting of countless individual ringlets, are believed to be remnants of shattered moons, comets, or asteroids, and they orbit the planet's equator. They provide a captivating spectacle for astronomers and space enthusiasts alike, offering valuable insights into the dynamics of planetary rings and the processes of celestial formation. Saturn's exploration, facilitated by spacecraft such as NASA's Cassini-Huygens mission, has revealed a wealth of information about its atmosphere, rings, and moons, enriching our understanding of the outer solar system and the mechanisms that govern its evolution. Saturn continues to captivate the imagination as one of the most visually stunning and scientifically intriguing planets in our cosmic neighborhood.''',
      bigImg:
          "https://i.pinimg.com/originals/eb/24/09/eb2409fa11b564c90d109813b8cd9533.gif",
      websiteLink: "https://en.wikipedia.org/wiki/Saturn",
    );
    SingleModalPlanet uranus = SingleModalPlanet(
      planetName: "Uranus",
      planetImg:
          "https://i.pinimg.com/564x/ca/47/cd/ca47cd3b63b26fc09ba32d838f1759b6.jpg",
      info:
          "A gas giant with a unique feature of rotating on its side, and it has a faint ring system and a bluish-green color due to methane in its atmosphere.",
      moreInfo:
          '''   Uranus, the seventh planet from the Sun, is a unique ice giant in our solar system. Named after the Greek god of the sky, Uranus is distinguished by its pale blue-green coloration and its peculiar rotational axis, which is tilted almost parallel to its orbital plane, causing it to appear to roll on its side as it orbits the Sun. With a diameter of approximately 50,724 kilometers (31,518 miles), Uranus is the third-largest planet in terms of size and the fourth-largest in terms of mass. Its atmosphere is primarily composed of hydrogen and helium, with traces of methane that give it its distinctive color. Uranus also possesses a system of faint rings and a retinue of moons, the most notable being Miranda, with its diverse and heavily cratered surface.
            
            Uranus is known for its extreme cold temperatures, with surface temperatures plunging to as low as -224°C (-371°F), making it one  of the coldest planets in the solar system. Despite its remote location, Uranus has been visited by only one spacecraft, Voyager 2, which conducted a flyby in 1986, providing valuable data and images of this distant ice giant. Uranus remains a subject of scientific  interest, with ongoing studies aimed at unraveling its atmospheric composition, internal structure, and peculiar rotational dynamics, offering  insights into the formation and evolution of ice giants in our cosmic neighborhood.''',
      bigImg:
          "https://i.pinimg.com/originals/47/5c/b3/475cb33b361c1f96ce03134e3bd7a27f.gif",
      websiteLink: "https://en.wikipedia.org/wiki/Uranus",
    );
    SingleModalPlanet neptune = SingleModalPlanet(
      planetName: "Neptune",
      planetImg:
          "https://i.pinimg.com/564x/28/41/14/284114e89a9ae8522bc5badf99d718c9.jpg",
      info:
          "The farthest planet from the Sun, characterized by its deep blue color and strong winds, and it has a faint ring system and several moons, including Triton.",
      moreInfo: '''
    Neptune, named after the Roman god of the sea, is the eighth and farthest planet from the Sun in our solar system. It is classified as an ice giant, along with Uranus, due to its composition primarily composed of icy materials such as water, ammonia, and methane, surrounding a rocky core. Neptune is known for its striking blue hue, attributed to the presence of methane  in its atmosphere, which absorbs red light and reflects blue. The planet has a diameter of about 49,244 kilometers (30,598 miles), making it slightly larger than Uranus. Despite its size, Neptune is not visible to the naked eye from Earth without the aid of a telescope due to its immense distance from the Sun.
        
        Neptune boasts several intriguing features, including its turbulent atmosphere characterized by strong winds, massive storms, and the fastest winds in the solar system, reaching speeds of up to 2,100 kilometers per hour (1,300 miles per hour). One of the most famous features on Neptune is the Great Dark Spot, a massive storm system reminiscent of Jupiter's Great Red Spot, though it has since disappeared, indicating the dynamic nature of Neptune's atmosphere. The planet also possesses a system of rings and a diverse array of moons, the largest of which is Triton, notable for its retrograde orbit and potential cryovolcanism. Neptune's exploration has been limited, with only one  spacecraft,NASA's Voyager 2, conducting a flyby in 1989, providing valuable data and images of this distant ice giant.
''',
      bigImg:
          "https://i.pinimg.com/originals/cb/e2/cc/cbe2cc03c3ca9be04d135f89f203e010.gif",
      websiteLink: "https://en.wikipedia.org/wiki/Neptune",
    );

    // insertPlanetsData(mercury);
    // insertPlanetsData(venus);
    // insertPlanetsData(earth);
    // insertPlanetsData(mars);
    // insertPlanetsData(jupyter);
    // insertPlanetsData(saturn);
    // insertPlanetsData(uranus);
    // insertPlanetsData(neptune);

    await fetchInitialValues();
  }

  Future<void> fetchInitialValues() async {
    planetsData = await fetchPlanetsData();
    print(planetsData);
    setState(() {});
  }

  Future<void> insertPlanetsData(SingleModalPlanet obj) async {
    final localDB = await database;

    localDB.insert(
      "Planets",
      obj.getPlanetMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<SingleModalPlanet>> fetchPlanetsData() async {
    final localDb = await database;

    List<Map<String, dynamic>> mapEntry = await localDb.query(
      "Planets",
    );

    return List.generate(mapEntry.length, (i) {
      return SingleModalPlanet(
        id: mapEntry[i]["id"],
        planetName: mapEntry[i]["planetName"],
        planetImg: mapEntry[i]["planetImg"],
        info: mapEntry[i]["info"],
        moreInfo: mapEntry[i]["moreInfo"],
        bigImg: mapEntry[i]["bigImg"],
        websiteLink: mapEntry[i]["websiteLink"],
      );
    });
  }

  @override
  void initState() {
    super.initState();
    initializeDatabase();
  }

  bool isinput = false;
  int currindex = 0;

  TextEditingController nameTextController = TextEditingController();
  TextEditingController infoTextController = TextEditingController();
  TextEditingController webLinkTextController = TextEditingController();

  void addplanets(bool doEdit, [SingleModalPlanet? singleModelObject]) async {
    if (nameTextController.text.trim().isNotEmpty &&
        infoTextController.text.trim().isNotEmpty) {
      if (!doEdit) {
        //tmp variable to store website link
        String webLink =
            "https://i.pinimg.com/564x/7a/74/4d/7a744dfaecbde7ff8db95f88831975a4.jpg";

        if (webLinkTextController.text.trim().isNotEmpty) {
          webLink = webLinkTextController.text.trim();
        }

        SingleModalPlanet obj = SingleModalPlanet(
          planetName: nameTextController.text.trim(),
          info: infoTextController.text.trim(),
          moreInfo: infoTextController.text.trim(),
          bigImg: webLink,
          planetImg: webLink,
          websiteLink: "https://en.wikipedia.org/wiki/Planet",
        );

        await insertPlanetsData(obj);
        planetsData = await fetchPlanetsData();

        setState(() {});
      }
    }

    clearControllers();
  }

  void clearControllers() {
    nameTextController.clear();
    infoTextController.clear();
  }

  void buildBottomsheet(bool doEdit, [SingleModalPlanet? singleModelObject]) {
    showModalBottomSheet(
        backgroundColor: Colors.black,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Insert Planet",
                    style: GoogleFonts.zenDots(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      controller: nameTextController,
                      style: TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                          labelText: "Enter Object Name",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          labelStyle: TextStyle(fontStyle: FontStyle.italic),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1)),
                          )),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      controller: webLinkTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                          labelText: "Paste planet img link from web.",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          labelStyle: TextStyle(fontStyle: FontStyle.italic),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1)),
                          )),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      controller: infoTextController,
                      maxLines: 4,
                      style: TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                        labelText: "Enter Object Description",
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        labelStyle: TextStyle(fontStyle: FontStyle.italic),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          borderSide: BorderSide(
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    height: 50,
                    width: 300,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(30)),
                    child: ElevatedButton(
                        onPressed: () {
                          if (!isinput) {
                            addplanets(doEdit, singleModelObject);
                          }
                          Navigator.of(context).pop();
                        },
                        style: const ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(
                                Color.fromARGB(255, 215, 85, 49))),
                        child: Text(
                          "Add Object",
                          style: GoogleFonts.zenDots(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                          ),
                        )),
                  ),
                  const SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.grey,
                ),
              ),
            ),
            ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: planetsData.length,
                separatorBuilder: (context, index) {
                  return const SizedBox(
                    height: 20,
                  );
                },
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      SingleModalObject object = SingleModalObject(
                        name: planetsData[index].planetName,
                        img: planetsData[index].planetImg,
                        info: planetsData[index].info,
                        moreInfo: planetsData[index].moreInfo,
                        bigImg: planetsData[index].bigImg,
                        websiteLink: planetsData[index].websiteLink,
                      );

                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return MoreInfoPage(object: object);
                      }));
                    },
                    child: Container(
                      margin: const EdgeInsets.only(left: 15, right: 15),
                      padding: const EdgeInsets.symmetric(
                          vertical: 16, horizontal: 22),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.white,
                          width: 3,
                        ),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20),
                        ),
                      ),
                      child: Row(
                        children: [
                          Image.network(
                            planetsData[index].planetImg,
                            width: 106,
                          ),
                          const SizedBox(
                            width: 10,
                          ),
                          Flexible(
                            child: Column(
                              children: [
                                Text(
                                  planetsData[index].planetName,
                                  style: GoogleFonts.luckiestGuy(
                                    fontSize: 20,
                                    color: Colors.white,
                                  ),
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  planetsData[index].info,
                                  style: GoogleFonts.zenDots(
                                      fontSize: 14,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            // clearControllers();
            buildBottomsheet(false);
          });
        },
        shape: const CircleBorder(),
        backgroundColor: Colors.grey,
        child: const Icon(
          color: Colors.white,
          Icons.add,
          size: 40.0,
        ),
      ),
    );
  }
}
