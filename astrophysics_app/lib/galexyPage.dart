import 'package:astrophysics_app/databaseFile.dart';
import 'package:astrophysics_app/moreInfoPage.dart';
import 'package:flutter/material.dart';
import 'package:astrophysics_app/requiredClasses.dart';

import 'package:google_fonts/google_fonts.dart';

import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

class GalaxyInfo extends StatefulWidget {
  const GalaxyInfo({super.key});

  @override
  State<StatefulWidget> createState() {
    return _GalaxyInfoState();
  }
}

class _GalaxyInfoState extends State {
  List<SingleModalGalaxy> galaxiesData = [];

  //global database instance
  dynamic database;

  Future<void> initializeDatabase() async {
    database = mycreateDatabaseFunction();
// Code to insert galexy data

    SingleModalGalaxy obj1 = SingleModalGalaxy(
      galaxyName: "Andromeda",
      galaxyImg:
          "https://i.pinimg.com/564x/e4/37/37/e43737b87afbf3619123c1c894478b6d.jpg",
      info:
          "The closest spiral galaxy to the Milky Way and the largest galaxy in the Local Group.",
      moreInfo:
          "The closest spiral galaxy to the Milky Way and the largest galaxy in the Local Group.",
      websiteLink: "",
      bigImg:
          "https://i.pinimg.com/564x/e4/37/37/e43737b87afbf3619123c1c894478b6d.jpg",
    );

    SingleModalGalaxy obj2 = SingleModalGalaxy(
      galaxyName: "Milky Way",
      galaxyImg:
          "https://i.pinimg.com/564x/da/86/38/da86387ac512e84c1bb90d1d6d68a305.jpg",
      info:
          "The galaxy in which our solar system resides, known for its spiral structure and vast collection of stars, planets, and other celestial objects.",
      moreInfo:
          "The galaxy in which our solar system resides, known for its spiral structure and vast collection of stars, planets, and other celestial objects.",
      bigImg:
          "https://i.pinimg.com/564x/da/86/38/da86387ac512e84c1bb90d1d6d68a305.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Milky_Way",
    );

    SingleModalGalaxy obj3 = SingleModalGalaxy(
      galaxyName: "Triangulum",
      galaxyImg:
          "https://i.pinimg.com/564x/5c/7c/6f/5c7c6f86b07097d3dabec8f1718c6d14.jpg",
      info:
          "Another member of the Local Group, smaller than the Milky Way and Andromeda galaxies but still visible with binoculars from Earth.",
      moreInfo:
          "Another member of the Local Group, smaller than the Milky Way and Andromeda galaxies but still visible with binoculars from Earth.",
      bigImg:
          "https://i.pinimg.com/564x/5c/7c/6f/5c7c6f86b07097d3dabec8f1718c6d14.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Triangulum",
    );
    SingleModalGalaxy obj4 = SingleModalGalaxy(
      galaxyName: "Sombrero",
      galaxyImg:
          "https://i.pinimg.com/564x/f4/ca/df/f4cadfb619ad531aab47198117574384.jpg",
      info:
          "Notable for its distinctive hat-shaped appearance caused by a prominent dust lane, located approximately 28 million light-years away in the constellation Virgo.",
      moreInfo:
          "Notable for its distinctive hat-shaped appearance caused by a prominent dust lane, located approximately 28 million light-years away in the constellation Virgo.",
      bigImg:
          "https://i.pinimg.com/564x/f4/ca/df/f4cadfb619ad531aab47198117574384.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Sombrero_Galaxy",
    );
    SingleModalGalaxy obj5 = SingleModalGalaxy(
      galaxyName: "Whirlpool",
      galaxyImg:
          "https://i.pinimg.com/564x/85/cd/c3/85cdc3e80eba21e8438783e8cdecc4c6.jpg",
      info:
          "An interacting grand-design spiral galaxy located about 23 million light-years away in the constellation Canes Venatici.",
      moreInfo:
          "An interacting grand-design spiral galaxy located about 23 million light-years away in the constellation Canes Venatici.",
      bigImg:
          "https://i.pinimg.com/564x/85/cd/c3/85cdc3e80eba21e8438783e8cdecc4c6.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Whirlpool_Galaxy",
    );

    SingleModalGalaxy obj6 = SingleModalGalaxy(
      galaxyName: "Large Magellanic Cloud ",
      galaxyImg:
          "https://i.pinimg.com/564x/43/19/63/4319632e3108cbfbf4d01104719cc826.jpg",
      info:
          "A satellite galaxy of the Milky Way, visible to the naked eye from the Southern Hemisphere, and containing regions of active star formation.",
      moreInfo:
          "A satellite galaxy of the Milky Way, visible to the naked eye from the Southern Hemisphere, and containing regions of active star formation.",
      bigImg:
          "https://i.pinimg.com/564x/43/19/63/4319632e3108cbfbf4d01104719cc826.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Large_Magellanic_Cloud",
    );
    SingleModalGalaxy obj7 = SingleModalGalaxy(
      galaxyName: "M87",
      galaxyImg:
          "https://i.pinimg.com/564x/7d/ff/7d/7dff7d83ec10c7b3c0b74eebe7d3f618.jpg",
      info:
          "A supergiant elliptical galaxy located in the Virgo Cluster, notable for containing the first-ever imaged black hole, located in its center.",
      moreInfo:
          "A supergiant elliptical galaxy located in the Virgo Cluster, notable for containing the first-ever imaged black hole, located in its center.",
      bigImg:
          "https://i.pinimg.com/564x/7d/ff/7d/7dff7d83ec10c7b3c0b74eebe7d3f618.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Messier_86",
    );
    SingleModalGalaxy obj8 = SingleModalGalaxy(
      galaxyName: "Centaurus A",
      galaxyImg:
          "https://i.pinimg.com/564x/90/e0/e9/90e0e9fe4d85128158b2d29a7e118aeb.jpg",
      info:
          "A peculiar galaxy with a prominent dust lane across its center, believed to be the result of a merger between a large elliptical galaxy and a smaller spiral galaxy.",
      moreInfo:
          "A peculiar galaxy with a prominent dust lane across its center, believed to be the result of a merger between a large elliptical galaxy and a smaller spiral galaxy.",
      bigImg:
          "https://i.pinimg.com/564x/90/e0/e9/90e0e9fe4d85128158b2d29a7e118aeb.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Centaurus_A",
    );

    SingleModalGalaxy obj9 = SingleModalGalaxy(
      galaxyName: "Pinwheel",
      galaxyImg:
          "https://i.pinimg.com/564x/98/15/2b/98152bdefe5c7a7482b46dda0600aafa.jpg",
      info:
          "A face-on spiral galaxy located in the constellation Ursa Major, known for its spiral arms and bright core, and visible with amateur telescopes.",
      moreInfo:
          "A face-on spiral galaxy located in the constellation Ursa Major, known for its spiral arms and bright core, and visible with amateur telescopes.",
      bigImg:
          "https://i.pinimg.com/564x/98/15/2b/98152bdefe5c7a7482b46dda0600aafa.jpg",
      websiteLink: "https://en.wikipedia.org/wiki/Pinwheel_Galaxy",
    );

    // insertGalaxiesData(obj1);
    // insertGalaxiesData(obj2);
    // insertGalaxiesData(obj3);
    // insertGalaxiesData(obj4);
    // insertGalaxiesData(obj5);
    // insertGalaxiesData(obj6);
    // insertGalaxiesData(obj7);
    // insertGalaxiesData(obj8);
    // insertGalaxiesData(obj9);

    await fetchInitialValues();
  }

  Future<void> fetchInitialValues() async {
    galaxiesData = await fetchGalaxiesData();
    print(galaxiesData);
    setState(() {});
  }

  Future<void> insertGalaxiesData(SingleModalGalaxy obj) async {
    final localDB = await database;

    localDB.insert(
      "Galaxies",
      obj.getGalaxyMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<SingleModalGalaxy>> fetchGalaxiesData() async {
    final localDb = await database;

    List<Map<String, dynamic>> mapEntry = await localDb.query(
      "Galaxies",
    );

    return List.generate(mapEntry.length, (i) {
      return SingleModalGalaxy(
        id: mapEntry[i]["id"],
        galaxyName: mapEntry[i]["galaxyName"],
        galaxyImg: mapEntry[i]["galaxyImg"],
        info: mapEntry[i]["info"],
        moreInfo: mapEntry[i]["moreInfo"],
        bigImg: mapEntry[i]["bigImg"],
        websiteLink: mapEntry[i]["websiteLink"],
      );
    });
  }

  @override
  void initState() {
    super.initState();
    initializeDatabase();
  }

  bool isinput = false;
  int currindex = 0;

  TextEditingController nameTextController = TextEditingController();
  TextEditingController infoTextController = TextEditingController();
  TextEditingController webLinkTextController = TextEditingController();

  void addoject(bool doEdit, [SingleModalGalaxy? singleModelObject]) async {
    if (nameTextController.text.trim().isNotEmpty &&
        infoTextController.text.trim().isNotEmpty) {
      if (!doEdit) {
        //tmp variable to store website link
        String webLink =
            "https://i.pinimg.com/564x/59/2b/80/592b806ac6bb8f8023de8be60b037a1f.jpg";

        if (webLinkTextController.text.trim().isNotEmpty) {
          webLink = webLinkTextController.text.trim();
        }

        SingleModalGalaxy obj = SingleModalGalaxy(
          galaxyName: nameTextController.text.trim(),
          info: infoTextController.text.trim(),
          moreInfo: infoTextController.text.trim(),
          bigImg: webLink,
          galaxyImg: webLink,
          websiteLink: "https://en.wikipedia.org/wiki/Galaxy",
        );

        await insertGalaxiesData(obj);
        galaxiesData = await fetchGalaxiesData();

        setState(() {});
      }
    }

    clearControllers();
  }

  void clearControllers() {
    nameTextController.clear();
    infoTextController.clear();
  }

  void buildBottomsheet(bool doEdit, [SingleModalGalaxy? singleModelObject]) {
    showModalBottomSheet(
        backgroundColor: Colors.black,
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return Padding(
            padding: EdgeInsets.only(
              left: 20,
              right: 20,
              bottom: MediaQuery.of(context).viewInsets.bottom,
            ),
            child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Insert Galexy",
                    style: GoogleFonts.zenDots(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 20,
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      controller: nameTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                          labelText: "Enter Object Name",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          labelStyle: TextStyle(fontStyle: FontStyle.italic),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1)),
                          )),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      controller: webLinkTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                          labelText: "Paste galaxy img link from web.",
                          hintStyle: TextStyle(
                            color: Colors.grey,
                          ),
                          labelStyle: TextStyle(fontStyle: FontStyle.italic),
                          focusedBorder: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.white)),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(15)),
                            borderSide: BorderSide(
                                color: Color.fromRGBO(255, 255, 255, 1)),
                          )),
                    ),
                  ),
                  const SizedBox(
                    height: 30,
                  ),
                  Container(
                    margin: const EdgeInsets.only(left: 15, right: 15),
                    child: TextFormField(
                      maxLines: 4,
                      controller: infoTextController,
                      style: const TextStyle(color: Colors.white),
                      decoration: const InputDecoration(
                        labelText: "Enter Object Description",
                        hintStyle: TextStyle(
                          color: Colors.grey,
                        ),
                        labelStyle: TextStyle(fontStyle: FontStyle.italic),
                        focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.white)),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                          borderSide: BorderSide(
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    height: 50,
                    width: 300,
                    decoration:
                        BoxDecoration(borderRadius: BorderRadius.circular(30)),
                    child: ElevatedButton(
                        onPressed: () {
                          if (!isinput) {
                            addoject(doEdit, singleModelObject);
                          }
                          Navigator.of(context).pop();
                        },
                        style: const ButtonStyle(
                            backgroundColor: MaterialStatePropertyAll(
                                Color.fromARGB(255, 215, 85, 49))),
                        child: Text(
                          "Add Object",
                          style: GoogleFonts.zenDots(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 20,
                          ),
                        )),
                  ),
                  const SizedBox(
                    height: 10,
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(20),
              child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: const Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.grey,
                ),
              ),
            ),
            ListView.separated(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: galaxiesData.length,
                separatorBuilder: (context, index) {
                  return const SizedBox(
                    height: 20,
                  );
                },
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      SingleModalObject object = SingleModalObject(
                        name: galaxiesData[index].galaxyName,
                        img: galaxiesData[index].galaxyImg,
                        info: galaxiesData[index].info,
                        moreInfo: galaxiesData[index].moreInfo,
                        bigImg: galaxiesData[index].bigImg,
                        websiteLink: galaxiesData[index].websiteLink,
                      );

                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return MoreInfoPage(object: object);
                      }));
                    },
                    child: Padding(
                      padding: const EdgeInsets.only(left: 15, right: 15),
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                            vertical: 16, horizontal: 22),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.white,
                            width: 3,
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(20),
                          ),
                        ),
                        child: Row(
                          children: [
                            Image.network(
                              galaxiesData[index].galaxyImg,
                              width: 106,
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: Column(
                                children: [
                                  Text(
                                    galaxiesData[index].galaxyName,
                                    style: GoogleFonts.luckiestGuy(
                                      fontSize: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    galaxiesData[index].info,
                                    style: GoogleFonts.zenDots(
                                        fontSize: 14,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            // clearControllers();
            buildBottomsheet(false);
          });
        },
        shape: const CircleBorder(),
        backgroundColor: Colors.grey,
        child: const Icon(
          color: Colors.white,
          Icons.add,
          size: 40.0,
        ),
      ),
    );
  }
}
