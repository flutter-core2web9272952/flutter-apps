class Employee {
  String id;
  String name;
  String userName;
  List<String> skill;

  Employee({
    required this.id,
    required this.name,
    required this.userName,
    required this.skill,
  });
}
