import 'package:api_demo/Assignment4/employee_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State {
  TextEditingController controller = TextEditingController();
  bool flg = false;
  // Widget showData() {
  //   if (empData != null) {
  //     return Row(
  //       mainAxisAlignment: MainAxisAlignment.center,
  //       children: [
  //         Text(Provider.of<EmployeeController>(context, listen: false)
  //             .data!
  //             .employeeName!),
  //         const SizedBox(width: 30),
  //         Text(
  //             "${Provider.of<EmployeeController>(context, listen: false).data!.employeeSalary}"),
  //       ],
  //     );
  //   } else {
  //     return const Center(child: Text("No Data"));
  //   }
  // }
  @override
  Widget build(BuildContext context) {
    var empData = Provider.of<EmployeeController>(context);

    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: const Text("API Binding"),
        ),
        body: Column(
          children: [
            const SizedBox(height: 30),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: 250,
                  child: TextFormField(
                    controller: controller,
                    decoration: InputDecoration(border: OutlineInputBorder()),
                  ),
                ),
              ],
            ),
            const SizedBox(height: 30),
            Consumer(
              builder: (context, value, child) {
                return flg
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(empData.data!.employeeName!),
                          const SizedBox(width: 30),
                          Text("${empData.data!.employeeSalary}"),
                        ],
                      )
                    : const Center(child: Text("No Data"));
              },
            ),
            const SizedBox(height: 30),
            ElevatedButton(
              onPressed: () async {
                await empData.postEmployeeData();
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text("Data is posted..."),
                  ),
                );
              },
              child: const Text("Post Data"),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            await empData.getEmployeeData(id: controller.text.trim());
            flg = true;
          },
          child: const Icon(Icons.add),
        ));
  }
}


/*

    Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return EmployeeController();
      },
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen(),
      ),
    );
  }


*/