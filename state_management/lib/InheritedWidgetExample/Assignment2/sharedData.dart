import 'package:flutter/material.dart';
import 'package:state_management/InheritedWidgetExample/Assignment2/modal.dart';

class SharedData extends InheritedWidget {
  Player data;

  SharedData({
    super.key,
    required this.data,
    required super.child,
  });

  static SharedData of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<SharedData>()!;
  }

  @override
  bool updateShouldNotify(SharedData oldWidget) {
    return data != oldWidget.data;
  }
}
