import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    double desktopThreshold = 1024.0;

    double topHeight1 = screenWidth < desktopThreshold
        ? screenHeight * 20 / 100
        : screenHeight * 35 / 100;

    double leftWidth1 = screenWidth < desktopThreshold
        ? screenWidth * 3 / 100
        : screenWidth * 10 / 100;

    double imgWidth = screenWidth < desktopThreshold
        ? screenWidth * 50 / 100
        : screenWidth * 42 / 100;

    double rightWidth2 = screenWidth < desktopThreshold
        ? screenWidth * 3 / 100
        : screenWidth * 10 / 100;

    return Scaffold(
      backgroundColor: const Color.fromRGBO(22, 22, 22, 1),
      body: SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
          ),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 2 / 100,
              ),
              Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 10 / 100,
                  ),
                  Text(
                    "SM",
                    style: GoogleFonts.orbitron(
                      color: Colors.amber,
                      fontSize: 35,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 47 / 100,
                  ),
                  Text(
                    "About",
                    style: GoogleFonts.orbitron(
                      color: const Color.fromRGBO(224, 224, 224, 1),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 3 / 100,
                  ),
                  Text(
                    "Skills",
                    style: GoogleFonts.orbitron(
                      color: const Color.fromRGBO(224, 224, 224, 1),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 3 / 100,
                  ),
                  Text(
                    "Projects",
                    style: GoogleFonts.orbitron(
                      color: const Color.fromRGBO(224, 224, 224, 1),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 3 / 100,
                  ),
                  Text(
                    "Contact",
                    style: GoogleFonts.orbitron(
                      color: const Color.fromRGBO(224, 224, 224, 1),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 2 / 100,
              ),
              SizedBox(
                // Wrapping Stack in a Container with a fixed height
                height: MediaQuery.of(context).size.height * 90 / 100,
                child: Stack(
                  children: [
                    Positioned(
                      left: leftWidth1,
                      top: topHeight1,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Text(
                                "Hi, I am ",
                                style: GoogleFonts.orbitron(
                                  color: Colors.white,
                                  fontSize: 24,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              Text(
                                "Sandesh Marathe ",
                                style: GoogleFonts.orbitron(
                                  color: Colors.amber,
                                  fontSize: 28,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Text(
                                "I am a ",
                                style: GoogleFonts.orbitron(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                "S",
                                style: GoogleFonts.orbitron(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w800,
                                  color: Colors.amber,
                                ),
                              ),
                              Text(
                                "oftware",
                                style: GoogleFonts.orbitron(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                " D",
                                style: GoogleFonts.orbitron(
                                  fontSize: 24,
                                  fontWeight: FontWeight.w800,
                                  color: Colors.amber,
                                ),
                              ),
                              Text(
                                "eveloper",
                                style: GoogleFonts.orbitron(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 5,
                          ),
                          Row(
                            children: [
                              RichText(
                                text: TextSpan(
                                  style: GoogleFonts.orbitron(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                  ),
                                  children: const [
                                    TextSpan(
                                      text: "I have expertise in ",
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                    TextSpan(
                                      text: "Flutter",
                                      style: TextStyle(
                                          color:
                                              Color.fromARGB(255, 24, 156, 238),
                                          fontWeight: FontWeight.w800,
                                          fontSize: 24),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      right: rightWidth2,
                      top: MediaQuery.of(context).size.height * 2 / 100,
                      child: Container(
                        width: imgWidth,
                        height: imgWidth,
                        decoration: const BoxDecoration(
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                              "assets/Images/bnw1.jpeg",
                            ),
                          ),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            gradient: RadialGradient(
                              stops: [0.5, 0.9999],
                              colors: [
                                Colors.transparent,
                                Color.fromRGBO(22, 22, 22, 1),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 300),
              Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 10 / 100,
                  ),
                  Text(
                    "SM",
                    style: GoogleFonts.orbitron(
                      color: Colors.amber,
                      fontSize: 35,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 47 / 100,
                  ),
                  Text(
                    "About",
                    style: GoogleFonts.orbitron(
                      color: const Color.fromRGBO(224, 224, 224, 1),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 3 / 100,
                  ),
                  Text(
                    "Skills",
                    style: GoogleFonts.orbitron(
                      color: const Color.fromRGBO(224, 224, 224, 1),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 3 / 100,
                  ),
                  Text(
                    "Projects",
                    style: GoogleFonts.orbitron(
                      color: const Color.fromRGBO(224, 224, 224, 1),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 3 / 100,
                  ),
                  Text(
                    "Contact",
                    style: GoogleFonts.orbitron(
                      color: const Color.fromRGBO(224, 224, 224, 1),
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
