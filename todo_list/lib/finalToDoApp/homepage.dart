import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State {
  List<Map> data = [
    {
      "title": "Lorem Ipsum is simply setting industry. ",
      "info":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
    },
    {
      "title": "Lorem Ipsum is simply setting industry. ",
      "info":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
    },
    {
      "title": "Lorem Ipsum is simply setting industry. ",
      "info":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
    },
    {
      "title": "Lorem Ipsum is simply setting industry. ",
      "info":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
    },
    {
      "title": "Lorem Ipsum is simply setting industry. ",
      "info":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
    },
    {
      "title": "Lorem Ipsum is simply setting industry. ",
      "info":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
    },
    {
      "title": "Lorem Ipsum is simply setting industry. ",
      "info":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
    },
    {
      "title": "Lorem Ipsum is simply setting industry. ",
      "info":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
    },
    {
      "title": "Lorem Ipsum is simply setting industry. ",
      "info":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
    }
  ];

  List<Color> colors = const [
    Color.fromRGBO(250, 232, 232, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1),
  ];

  int indx = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
      ),
      appBar: AppBar(
        title: Text(
          "To-do list",
          style: GoogleFonts.quicksand(
              textStyle: const TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 30,
                  color: Colors.white)),
        ),
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
      ),
      body: ListView.builder(
        itemCount: data.length,
        itemBuilder: (buildContext, index) {
          return Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              boxShadow: const [
                BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, .1),
                    offset: Offset(0, 10),
                    blurRadius: 20,
                    spreadRadius: 1),
              ],
              color: colors[index % colors.length],
              borderRadius: const BorderRadius.all(Radius.circular(10)),
            ),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: Text(
                      data[index]["title"],
                      style: GoogleFonts.quicksand(
                        textStyle: const TextStyle(
                          color: Color.fromRGBO(0, 0, 0, 1),
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                children: [
                  Container(
                    margin: const EdgeInsets.all(10),
                    padding: const EdgeInsets.all(20),
                    decoration: const BoxDecoration(
                        color: Colors.white, shape: BoxShape.circle),
                    child: Image.asset(
                      "assets/displayImage.png",
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: Text(
                      data[index]["info"],
                      style: GoogleFonts.quicksand(
                        textStyle: const TextStyle(
                          color: Color.fromRGBO(84, 84, 84, 1),
                          fontSize: 13,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    data[index]["date"],
                    style: GoogleFonts.quicksand(
                      textStyle: const TextStyle(
                        color: Color.fromRGBO(84, 84, 84, 1),
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  SizedBox(
                    child: Row(
                      children: [
                        IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.edit_outlined,
                            color: Color.fromRGBO(2, 167, 177, 1),
                          ),
                        ),
                        IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.delete_outline,
                            color: Color.fromRGBO(2, 167, 177, 1),
                          ),
                        )
                      ],
                    ),
                  )
                ],
              )
            ]),
          );
        },
      ),
    );
  }
}
