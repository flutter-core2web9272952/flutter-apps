import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State<StatefulWidget> createState() => _Assignment2();
}

class _Assignment2 extends State {
  // Pointer to iterate over list
  int curr = 0;

  final List<List<String>> _questions = [
    [
      "What is the primary function of the Scaffold widget in Flutter?",
      "Manage app state",
      "Handle user input",
      "Define the app's basic structure",
      "Create animations",
      "3"
    ],
    [
      "What does the setState method do in Flutter?",
      "Set the app's initial state",
      "Trigger a rebuild of the widget tree",
      "Define the app's layout",
      "Animate widgets",
      "2"
    ],
    [
      "In Flutter, which widget is commonly used for handling user gestures like taps?",
      "GestureDetector",
      "InkWell",
      "RaisedButton",
      "FlatButton",
      "1"
    ],
    [
      "Which Flutter widget is used to display a single fixed-size box?",
      "Container",
      "Row",
      "Column",
      "Stack",
      "1"
    ],
    [
      "What is the purpose of the main function in a Flutter app?",
      "Define the app's layout",
      "Start the app's main event loop",
      "Handle user input",
      "Manage app state",
      "2"
    ],
    [
      "How do you navigate to a new screen in Flutter?",
      "navigateToScreen()",
      "pushReplacement()",
      "changeScreen()",
      "Navigator.push()",
      "4"
    ],
    [
      "What is the purpose of the ListView widget in Flutter?",
      "Display a list of items in a scrollable view",
      "Create a grid layout",
      "Manage app state",
      "Handle user input",
      "1"
    ],
    [
      "Which property is used to define the alignment of children in a Column or Row?",
      "spacing",
      "alignment",
      "mainAxisAlignment",
      "crossAxisAlignment",
      "3"
    ],
    [
      "What is the purpose of the async keyword in Dart/Flutter?",
      "Define asynchronous functions",
      "Declare global variables",
      "Create widgets",
      "Handle exceptions",
      "1"
    ],
    [
      "Which widget is used for displaying an image in Flutter?",
      "ImageBox",
      "ImageView",
      "Image",
      "Picture",
      "3"
    ],
  ];

  Color rightColor = Colors.blue;
  Color wrongColor = Colors.red;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Quiz")),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            width: double.infinity,
            padding: const EdgeInsets.only(top: 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'Question :',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text('${curr + 1}/${_questions.length}')
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Question ${curr + 1}:',
                      style: TextStyle(fontWeight: FontWeight.w700),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Text(
                      '${_questions[curr][0]}',
                      softWrap: true,
                    )
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                width: 300,
                height: 40,
                child: ElevatedButton(
                  onPressed: () {
                    setState(() {
                      rightColor = Colors.green;
                    });
                  },
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(rightColor)),
                  child: Text(_questions[curr][1]),
                ),
              ),
              const SizedBox(height: 20),
              SizedBox(
                width: 300,
                height: 40,
                child: ElevatedButton(
                    onPressed: () {}, child: Text(_questions[curr][2])),
              ),
              const SizedBox(height: 20),
              SizedBox(
                width: 300,
                height: 40,
                child: ElevatedButton(
                    onPressed: () {}, child: Text(_questions[curr][3])),
              ),
              const SizedBox(height: 20),
              SizedBox(
                width: 300,
                height: 40,
                child: ElevatedButton(
                    onPressed: () {}, child: Text(_questions[curr][4])),
              )
            ],
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () {
            setState(() {
              curr = ++curr % _questions.length;

              //dummy code remove it
              rightColor = Colors.blue;
            });
          },
          child: const Icon(Icons.arrow_forward)),
    );
  }
}
