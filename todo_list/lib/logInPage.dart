import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  const Login({super.key});

  @override
  State<StatefulWidget> createState() {
    print("inside createState");

    return _LoginState();
  }
}

class _LoginState extends State {
  int x = 0;

  bool screenFlg = true;

  Widget showScreen() {
    if (screenFlg) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.red,
        ),
        body: Center(
            child: Column(
          children: [
            ElevatedButton(
                onPressed: () {
                  x += 1;
                  setState(() {
                    print("inside setState");
                  });
                },
                child: const Text("Click")),
            const SizedBox(
              height: 20,
            ),
            Text("X = $x"),
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(
                onPressed: () {
                  screenFlg = false;
                  setState(() {});
                },
                child: const Text("change Screen"))
          ],
        )),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.black,
        ),
        body: Center(
            child: Column(
          children: [
            const SizedBox(
              height: 30,
            ),
            ElevatedButton(
                onPressed: () {
                  screenFlg = true;
                  setState(() {});
                },
                child: const Text("change Screen"))
          ],
        )),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    print("Inside initState");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("inside didChangedDependencies");
  }

  @override
  void deactivate() {
    print("inside deactivate");
    super.deactivate();
  }

  @override
  void didUpdateWidget(StatefulWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
    print("In didChangedWidget");
  }

  @override
  void dispose() {
    print("inside dispose");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print("inside build");
    return showScreen();
  }
}
