import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management/Assignment1/controllers/product_controller.dart';

class DisplyProduct extends StatefulWidget {
  const DisplyProduct({super.key});

  @override
  State<StatefulWidget> createState() {
    return _DisplyProductState();
  }
}

class _DisplyProductState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 25),
          alignment: Alignment.bottomCenter,
          width: 300,
          height: 480,
          decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
              bottomRight: Radius.circular(5),
              bottomLeft: Radius.circular(5),
            ),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, 0),
                  spreadRadius: 0,
                  blurRadius: 10,
                  color: Color.fromARGB(94, 0, 0, 0))
            ],
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Image.network(
                Provider.of<ProviderProductController>(context).obj.productImg,
                width: 260,
                height: 320,
                fit: BoxFit.cover,
              ),
              const SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    Provider.of<ProviderProductController>(context)
                        .obj
                        .productName,
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                    decoration: BoxDecoration(
                      color: const Color.fromARGB(101, 255, 235, 59),
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: Text(
                      "\$ ${Provider.of<ProviderProductController>(context).obj.productPrice}",
                      style: const TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Consumer(
                    builder: (context, value, child) {
                      return IconButton(
                        onPressed: () {
                          Provider.of<ProviderProductController>(context,
                                  listen: false)
                              .toggleFevorite();
                        },
                        icon: Provider.of<ProviderProductController>(context)
                                .obj
                                .isfavorite
                            ? const Icon(Icons.favorite)
                            : const Icon(Icons.favorite_outline),
                      );
                    },
                  ),
                  Row(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Provider.of<ProviderProductController>(context,
                                  listen: false)
                              .decrementQuantity();
                        },
                        child: const Icon(Icons.remove),
                      ),
                      const SizedBox(width: 5),
                      Consumer(
                        builder: (create, value, child) {
                          return Text(
                            "${Provider.of<ProviderProductController>(context).obj.quantity}",
                          );
                        },
                      ),
                      const SizedBox(width: 5),
                      GestureDetector(
                        onTap: () {
                          Provider.of<ProviderProductController>(context,
                                  listen: false)
                              .incrementQuantity();
                        },
                        child: const Icon(Icons.add),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}


/*

  https://i.pinimg.com/564x/de/2d/02/de2d021e61235ccfcd81fa355f6790fc.jpg

*/