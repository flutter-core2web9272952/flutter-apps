import 'package:flutter/material.dart';
import 'package:state_management/InheritedWidgetExample/Assignment2/sharedData.dart';

class FirstPage extends StatefulWidget {
  const FirstPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _FirstPageState();
  }
}

class _FirstPageState extends State {
  TextEditingController _playerNameController = TextEditingController();
  TextEditingController _playerCountryController = TextEditingController();
  TextEditingController _playerIPLTeamController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    SharedData sharedObject = SharedData.of(context);

    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 250,
                height: 40,
                child: TextFormField(
                  controller: _playerNameController,
                  decoration: const InputDecoration(
                    hintText: "Player Name",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 250,
                height: 40,
                child: TextFormField(
                  controller: _playerCountryController,
                  decoration: const InputDecoration(
                    hintText: "Player Country",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 250,
                height: 40,
                child: TextFormField(
                  controller: _playerIPLTeamController,
                  decoration: const InputDecoration(
                    hintText: "Player IPL Team",
                    border: OutlineInputBorder(),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          GestureDetector(
            onTap: () {
              sharedObject.data.playerName = _playerNameController.text;
              sharedObject.data.playerCountry = _playerCountryController.text;
              sharedObject.data.playerIplTeam = _playerIPLTeamController.text;
              setState(() {});

              Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => const InfoPage()));
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              decoration: BoxDecoration(
                color: Colors.purple.shade100,
                borderRadius: BorderRadius.circular(10),
              ),
              child: const Text("Submit"),
            ),
          )
        ],
      ),
    );
  }
}

class InfoPage extends StatefulWidget {
  const InfoPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _InfoPageState();
  }
}

class _InfoPageState extends State {
  @override
  Widget build(BuildContext context) {
    SharedData sharedObject = SharedData.of(context);
    print(sharedObject.data);

    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                sharedObject.data.playerName,
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                sharedObject.data.playerCountry,
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                sharedObject.data.playerIplTeam,
                style: const TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                ),
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
        ],
      ),
    );
  }
}
