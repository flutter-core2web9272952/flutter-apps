import 'package:flutter/material.dart';
// import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

dynamic database;

class Employee {
  final int empId;
  final String name;
  final double salary;

  Employee({required this.empId, required this.name, required this.salary});

  Map<String, dynamic> employeeMap() {
    return {
      "empId": empId,
      "name": name,
      "sal": salary,
    };
  }
}

Future<void> insertEmployeeData(Employee emp) async {
  final localDB = await database;

  localDB.insert(
    "employee",
    emp.employeeMap(),
    conflictAlgorithm: ConflictAlgorithm.replace,
  );
}

Future<void> deleteEmployeeData(Employee emp) async {
  final localDb = await database;

  localDb.delete(
    "employee",
    where: "empId = ?",
    whereArgs: [emp.empId],
  );
}

Future<List<Employee>> fetchEmployeeData() async {
  final localDb = await database;

  List<Map<String, dynamic>> mapEntry = await localDb.query(
    "employee",
  );

  return List.generate(mapEntry.length, (i) {
    return Employee(
        empId: mapEntry[i]["empId"],
        name: mapEntry[i]["name"],
        salary: mapEntry[i]["sal"]);
  });
}

void main() async {
  runApp(const MainApp());

  databaseFactory = databaseFactoryFfi;

  print(await getDatabasesPath());

  database = databaseFactory.openDatabase(
      join(await getDatabasesPath(), "EmployeeDB.db"),
      options: OpenDatabaseOptions(
        version: 1,
        onCreate: (db, version) {
          db.execute('''
      CREATE TABLE employee(
        empId INTEGER
        PRIMARY KEY,
        name TEXT,
        sal REAL)''');
        },
      ));

  Employee emp1 = Employee(empId: 1, name: "Sandesh", salary: 45);
  Employee emp2 = Employee(empId: 2, name: "Ayush", salary: 45);
  Employee emp3 = Employee(empId: 3, name: "Avishkar", salary: 46);
  Employee emp4 = Employee(empId: 4, name: "Suraj", salary: 45);
  Employee emp5 = Employee(empId: 5, name: "Suyog", salary: 45);
  Employee emp6 = Employee(empId: 6, name: "Omkar", salary: 45);
  Employee emp7 = Employee(empId: 7, name: "Manik", salary: 45);

  await insertEmployeeData(emp1);
  await insertEmployeeData(emp2);
  await insertEmployeeData(emp3);
  await insertEmployeeData(emp4);
  await insertEmployeeData(emp5);
  await insertEmployeeData(emp6);
  await insertEmployeeData(emp7);

  List<Employee> retData = await fetchEmployeeData();

  for (int i = 0; i < retData.length; i++) {
    print(
        "empID: ${retData[i].empId} name: ${retData[i].name} sal: ${retData[i].salary}");
  }

  await deleteEmployeeData(emp3);
  await deleteEmployeeData(emp4);

  retData = await fetchEmployeeData();

  for (int i = 0; i < retData.length; i++) {
    print(
        "empID: ${retData[i].empId} name: ${retData[i].name} sal: ${retData[i].salary}");
  }
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Center(
          child: Text('Hello World!'),
        ),
      ),
    );
  }
}
