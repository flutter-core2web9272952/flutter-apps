import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomePage();
  }
}

class _HomePage extends State<HomePage> {
  List<List<String>> userInfo = [
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://c4.wallpaperflare.com/wallpaper/139/444/1000/cristiano-ronaldo-wallpaper-preview.jpg",
    ],
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://wallpaperaccess.com/full/780073.jpg",
    ],
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://i2-prod.mirror.co.uk/incoming/article14152752.ece/ALTERNATES/s1200/0_Juventus-FC-vs-Atletico-Madrid-Turin-Italy-12-Mar-2019.jpg",
    ],
    [
      "virat.kohli",
      "https://imagevars.gulfnews.com/2022/09/12/Anushka-Virat1_18333434bb6_original-ratio.jpg",
      "https://media.gettyimages.com/id/1794726292/photo/mumbai-india-virat-kohli-of-india-celebrates-after-scoring-a-century-overtaking-sachin.jpg?s=612x612&w=0&k=20&c=hrgu0NHIY0qh4AVuFJPg3l-wUhmHbMV9n8qb6m5KE-0=",
    ],
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://media.gettyimages.com/id/962792890/photo/kiev-ukraine-cristiano-ronaldo-of-real-madrid-lifts-the-uefa-champions-league-trophy-following.jpg?s=612x612&w=0&k=20&c=F51xOktZy_gdCC-UpEo3cq4ZsaAnAi9e2XKRap_bCss=",
    ],
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://media.gettyimages.com/id/1163338011/photo/singapore-singapore-cristiano-ronaldo-of-juventus-celebrates-scoring-his-sides-second-goal.jpg?s=612x612&w=0&k=20&c=Nw1-RQYYYYpHp6pkgIYCJ_Uq2Y3Yv0Re3XMCnap4U1g=",
    ],
    [
      "virat.kohli",
      "https://imagevars.gulfnews.com/2022/09/12/Anushka-Virat1_18333434bb6_original-ratio.jpg",
      "https://media.gettyimages.com/id/1012037708/photo/birmingham-england-india-batsman-virat-kohli-celebrates-his-century-during-day-two-of-the.jpg?s=612x612&w=0&k=20&c=i4n2bcCCKgMeFEc0jT0JQ5FkRsIUZKXarARHmSoLy_A=",
    ],
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://media.gettyimages.com/id/1384775591/photo/manchester-england-cristiano-ronaldo-of-manchester-united-celebrates-after-scoring-their.jpg?s=612x612&w=0&k=20&c=36baa6lz-MJxUggxz-TtJm2UKp5VLxi43393ccZhugg=",
    ],
    [
      "cristiano",
      "https://pbs.twimg.com/media/FQ7KxCVXIAMILRB.jpg",
      "https://media.gettyimages.com/id/1290158744/photo/barcelona-spain-cristiano-ronaldo-of-juventus-f-c-is-put-under-pressure-by-lionel-messi-of.jpg?s=612x612&w=0&k=20&c=TrT8WOsLB0VUjs5guNMXqVaU5X-FaKfWDZhqXyYyhbE=",
    ],
    [
      "virat.kohli",
      "https://imagevars.gulfnews.com/2022/09/12/Anushka-Virat1_18333434bb6_original-ratio.jpg",
      "https://media.gettyimages.com/id/1472909629/photo/ahmedabad-india-virat-kohli-of-india-celebrates-after-scoring-his-century-during-day-four-of.jpg?s=612x612&w=0&k=20&c=FRS6QUbu-RZ8UMBYe0iU2UINh0aS1oKx77YY0JjyU5k=",
    ],
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: const Text(
          "Instagram",
          style: TextStyle(
              fontStyle: FontStyle.italic, color: Colors.black, fontSize: 25),
        ),
        actions: const [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.favorite_border_outlined,
                color: Colors.black,
              ),
              SizedBox(
                width: 10,
              ),
              Icon(
                Icons.mark_chat_unread_outlined,
                color: Colors.black,
              )
            ],
          )
        ],
      ),
      body: ListView.builder(
        itemCount: userInfo.length,
        itemBuilder: (context, index) {
          return Container(
            decoration: const BoxDecoration(
              border: Border(top: BorderSide(width: 1, color: Colors.grey)),
            ),
            child: SinglePostWidget(
              userName: userInfo[index][0],
              profilePhoto: userInfo[index][1],
              postPhoto: userInfo[index][2],
            ),
          );
        },
      ),
    );
  }
}

class SinglePostWidget extends StatefulWidget {
  final String userName;
  final String profilePhoto;
  final String postPhoto;

  const SinglePostWidget({
    super.key,
    required this.userName,
    required this.profilePhoto,
    required this.postPhoto,
  });
  @override
  State<StatefulWidget> createState() => _SinglePostWidgetState();
}

class _SinglePostWidgetState extends State<SinglePostWidget> {
  bool isLike = false;
  bool isBookMark = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.all(4.0),
                  child: Row(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(top: 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              padding: const EdgeInsets.all(1.0),
                              margin: const EdgeInsets.only(right: 20),
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(width: 2.5, color: Colors.grey),
                              ),
                              child: CircleAvatar(
                                radius: 20.0,
                                backgroundImage:
                                    NetworkImage(widget.profilePhoto),
                              ),
                            ),
                            Text(
                              widget.userName,
                              style: const TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.w700),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        width: 160,
                      ),
                      IconButton(
                          onPressed: () {}, icon: const Icon(Icons.more_vert))
                    ],
                  ),
                )
              ],
            )
          ],
        ),
        Image.network(
          widget.postPhoto,
          width: double.infinity,
          errorBuilder:
              (BuildContext context, Object error, StackTrace? stackTrace) {
            return Container(
              height: 300,
              color: Colors.grey,
              child: Center(
                  child: Icon(
                Icons.error,
                color: Colors.red,
                size: 30,
              )),
            );
          },
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                IconButton(
                    onPressed: () {
                      setState(() {
                        isLike = !isLike;
                      });
                    },
                    icon: isLike
                        ? const Icon(
                            Icons.favorite,
                            color: Colors.red,
                          )
                        : const Icon(Icons.favorite_border_outlined)),
                IconButton(
                    onPressed: () {}, icon: const Icon(Icons.comment_outlined)),
                IconButton(onPressed: () {}, icon: const Icon(Icons.send)),
              ],
            ),
            IconButton(
                onPressed: () {
                  setState(() {
                    isBookMark = !isBookMark;
                  });
                },
                icon: isBookMark
                    ? const Icon(
                        Icons.bookmark,
                      )
                    : const Icon(
                        Icons.turned_in_not_outlined,
                      ))
          ],
        ),
      ],
    );
  }
}
