import 'package:astrophysics_app/asteroidsPage.dart';
import 'package:astrophysics_app/galexyPage.dart';
import 'package:astrophysics_app/othersPage.dart';
import 'package:astrophysics_app/planetsInfo.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LandingPageState();
  }
}

class _LandingPageState extends State<LandingPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      // If we want to use SliverAppBars or any Sliver related Widget then we need CustomeScrollView
      // it provides us  a flexibility

      body: CustomScrollView(
        slivers: [
          // We have used SliverAppBar because it gives us chance to make flexible appbar
          // and it provides parallax effect bydefault.
          SliverAppBar(
            expandedHeight: 600,
            flexibleSpace: FlexibleSpaceBar(
              background: Container(
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      "https://i.pinimg.com/236x/cc/d8/b1/ccd8b103e523595b9bace397b60f1723.jpg",
                    ),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Text("Explore",
                              style: GoogleFonts.luckiestGuy(
                                  fontSize: 64, color: Colors.white)),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "The",
                            style: GoogleFonts.luckiestGuy(
                                fontSize: 64, color: Colors.white),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            "Universe",
                            style: GoogleFonts.luckiestGuy(
                                fontSize: 64, color: Colors.white),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate.fixed(
              [
                Padding(
                  padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PlanetsInfo()));
                    },
                    child: Container(
                      height: 230,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 5),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromARGB(209, 37, 37, 37),
                              offset: Offset(0, 10),
                              spreadRadius: 1,
                              blurRadius: 20.0)
                        ],
                        color: Colors.white,
                        // border: Border.all(),
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                      ),

                      // First child of the second container

                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          "https://i.pinimg.com/564x/19/8c/db/198cdbd83f1b88313f9a3da32b389d40.jpg",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const GalaxyInfo()));
                    },
                    child: Container(
                      height: 230,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 5),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromARGB(209, 37, 37, 37),
                              offset: Offset(0, 10),
                              spreadRadius: 1,
                              blurRadius: 20.0)
                        ],
                        color: Colors.white,
                        // border: Border.all(),
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                      ),

                      // First child of the second container

                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          "https://i.pinimg.com/564x/2d/50/ea/2d50ea3a7d16c65bad846c2903159063.jpg",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 15, left: 20, right: 20),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const AsteroidInfo()));
                    },
                    child: Container(
                      height: 230,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 5),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromARGB(209, 37, 37, 37),
                              offset: Offset(0, 10),
                              spreadRadius: 1,
                              blurRadius: 20.0)
                        ],
                        color: Colors.white,
                        // border: Border.all(),
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                      ),

                      // First child of the second container

                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          "https://i.pinimg.com/564x/ee/e1/e2/eee1e26849ff971a32014950ee40aafa.jpg",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 15, left: 20, right: 20, bottom: 30),
                  child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const OtherInfo()));
                    },
                    child: Container(
                      height: 230,
                      decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 5),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromARGB(209, 37, 37, 37),
                              offset: Offset(0, 10),
                              spreadRadius: 1,
                              blurRadius: 20.0)
                        ],
                        color: Colors.white,
                        // border: Border.all(),
                        borderRadius: BorderRadius.all(Radius.circular(14)),
                      ),

                      // First child of the second container

                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(8.0),
                        child: Image.network(
                          "https://i.pinimg.com/736x/72/f7/9b/72f79b794ac3b9c4049b8c15f39d0ea1.jpg",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
