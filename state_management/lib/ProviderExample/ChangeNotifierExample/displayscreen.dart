import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:state_management/ProviderExample/ChangeNotifierExample/modalClass.dart';

class ParentWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Info();
  }
}

class Info extends StatefulWidget {
  const Info({super.key});

  @override
  State<StatefulWidget> createState() {
    return _InfoState();
  }
}

class _InfoState extends State {
  @override
  Widget build(BuildContext context) {
    CompanyModal obj = Provider.of<CompanyModal>(context);

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(obj.compName),
            const SizedBox(
              height: 20,
            ),
            Text("${obj.empCount}"),
            const SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Provider.of<CompanyModal>(context, listen: false).empCount +=
                    10;

                obj.notifyListeners();
              },
              child: Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: const Text(
                  "Click",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
