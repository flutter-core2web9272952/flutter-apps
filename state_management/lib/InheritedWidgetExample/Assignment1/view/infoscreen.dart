import 'package:flutter/material.dart';
import 'package:state_management/InheritedWidgetExample/Assignment1/sharedData.dart';
import 'package:state_management/InheritedWidgetExample/Assignment1/view/skillscreen.dart';

class InfoPage extends StatefulWidget {
  const InfoPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _InfoPageState();
  }
}

class _InfoPageState extends State {
  @override
  Widget build(BuildContext context) {
    SharedData sharedObject = SharedData.of(context);

    print(sharedObject.data.id);
    print(sharedObject.data.name);
    print(sharedObject.data.userName);
    print(sharedObject.data.skill);

    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          const SizedBox(
            height: 50,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Id:",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Color.fromARGB(169, 0, 0, 0)),
              ),
              const SizedBox(
                width: 20,
              ),
              Text(
                sharedObject.data.id,
                style:
                    const TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              ),
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Name:",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Color.fromARGB(169, 0, 0, 0)),
              ),
              const SizedBox(
                width: 20,
              ),
              Text(
                sharedObject.data.name,
                style:
                    const TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              ),
            ],
          ),
          const SizedBox(
            height: 5,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Username:",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Color.fromARGB(169, 0, 0, 0)),
              ),
              const SizedBox(
                width: 20,
              ),
              Text(
                sharedObject.data.userName,
                style:
                    const TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                "Skills:",
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Color.fromARGB(169, 0, 0, 0)),
              ),
              const SizedBox(
                width: 20,
              ),
              Text(
                "${sharedObject.data.skill}",
                style:
                    const TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context)
                  .push(MaterialPageRoute(builder: (context) => SkillPage()));
            },
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
              decoration: BoxDecoration(
                  color: Colors.purple.shade100,
                  borderRadius: BorderRadius.circular(10)),
              child: const Text(
                "Add Skill",
                style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500),
              ),
            ),
          )
        ],
      ),
    );
  }
}
